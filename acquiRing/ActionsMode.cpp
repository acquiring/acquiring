#include "ActionsMode.h"
#include "Util.h"
#include "GUI\MainPage.xaml.h"


using namespace acquiRing;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::Data::Json;

Vector<ActionsMode^>^ ActionsMode::m_modeList = ref new Vector<ActionsMode^>();

ActionsMode::ActionsMode() {}

acquiRing::ActionsMode::ActionsMode(int id, String ^ name) {
	this->ID = id;
	this->Name = name;
	this->m_gestureList = ref new Vector<Gesture^>();
	for each (auto gesture in Gesture::GestureList) {
		auto g = ref new Gesture(gesture->ID, "");
		if(gesture->Operation != nullptr)
			g->Operation = ref new Plugin(gesture->Operation->UID);
		this->m_gestureList->Append(g);
	}
	
	this->Deleted = false;
}

Gesture ^ acquiRing::ActionsMode::getGestureByID(int gID) {
	for each (auto gesture in this->GestureList) {
		if (gID == gesture->ID) 
			return gesture;
	}
	return nullptr;
}

void acquiRing::ActionsMode::Delete() {
	this->Deleted = true;

	ActionsMode::Save();

	MainPage::UpdateGestureListView();
}

ActionsMode ^ acquiRing::ActionsMode::getActionModeByID(int amID) {
	for each (auto am in ActionsMode::ModeList) {
		if (am->ID == amID) return am;
	}
	return nullptr;
}

int acquiRing::ActionsMode::Load() {
	auto modesStr = LoadTxtFile("modes.json");
	if (modesStr.empty()) return 0;

	JsonArray^ mapValue = ref new JsonArray();
	if (JsonArray::TryParse(ss2ps(modesStr), &mapValue)) {
		auto vec = mapValue->GetView();

		ActionsMode::ModeList->Clear();
		std::for_each(begin(vec), end(vec), [](IJsonValue^ M) {
			auto actionMode = ref new ActionsMode(
				(int)M->GetObject()->GetNamedNumber("ID"),
				M->GetObject()->GetNamedString("NAME"));

			auto jarrayGestures = M->GetObject()->GetNamedArray("GESTURES");

			auto vecGesture = jarrayGestures->GetView();
			IVector<Gesture^>^ gList = ref new Vector<Gesture^>();
			std::for_each(begin(vecGesture), end(vecGesture), [gList](IJsonValue^ MG) {
				int gestureID = (int)MG->GetObject()->GetNamedNumber("GID");
				String^ pluginID = MG->GetObject()->GetNamedString("PID");
				auto gesture = ref new Gesture(gestureID, "");
				gesture->Operation = ref new Plugin(pluginID);
				gList->Append(gesture);
			});
			actionMode->SetGestureList(gList);
			ActionsMode::ModeList->Append(actionMode);
		});

	}

	MainPage::UpdateModeListView();

	return ActionsMode::ModeList->Size;
}

void acquiRing::ActionsMode::Save() {
	JsonArray^ jarray = ref new JsonArray();
	for each (auto mode in ModeList) {
		if (mode->Deleted == false) {
			JsonObject^ jobj = ref new JsonObject();
			jobj->Insert("ID", JsonValue::CreateNumberValue(mode->ID));
			jobj->Insert("NAME", JsonValue::CreateStringValue(mode->Name));

			JsonArray^ jarrayGestures = ref new JsonArray();
			for each (auto gesture in mode->GestureList) {
				if (gesture->Operation != nullptr) {
					JsonObject^ jobjGesture = ref new JsonObject();
					jobjGesture->Insert("GID", JsonValue::CreateNumberValue(gesture->ID));
					jobjGesture->Insert("PID", JsonValue::CreateStringValue(gesture->Operation->UID));
					jarrayGestures->Append(jobjGesture);
				}
			}
			jobj->Insert("GESTURES", jarrayGestures);
			jarray->Append(jobj);
		}
	}

	String^ jsonString = jarray->Stringify();
	SaveTxtFile("modes.json", jsonString);
}
