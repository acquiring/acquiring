#include "CommunicationModule.h"

#include "metawear\peripheral\led.h"
#include "metawear\sensor\accelerometer.h"
#include "metawear\sensor\accelerometer_mma8452q.h"
#include "metawear\sensor\accelerometer_bosch.h"
#include "metawear\sensor\gpio.h"
#include "metawear\sensor\gyro_bmi160.h"
#include "metawear\core\datasignal.h"
#include "metawear\core\types.h"
#include "metawear\core\timer.h"
#include "metawear\core\logging.h"
#include "metawear\core\event.h"
#include "metawear\core\debug.h"
#include "metawear\core\settings.h"

#include "Classifier\ClassifierAdapter.h"
#include "GUI/MainPage.xaml.h"
#include "GUI/TrainPage.xaml.h"

#include "DS.h"
#include "Util.h"
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <time.h>

using namespace acquiRing;

using namespace std;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Platform;
using namespace Platform::Collections;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::Storage::Streams;
using namespace Windows::UI::Core;


Map<Guid, String^> mwDeviceInfoChars;
Map<Guid, String^> DEVICE_INFO_NAMES;
unordered_map<Signal, MblMwDataSignal*> signals;

MblMwLogDownloadHandler log_down;
MblMwBtleConnection btle_conn;
MblMwTimer* gpio_read_timer;
MblMwMetaWearBoard *board;

GattDeviceService^ mwGattService;
GattCharacteristic^ mwNotifyChar;

bool LOG_STREAM = true, sensor_enable, SAVE_DATA = false, GAME_MODE = false;
int debug_cnt = 0, GPIO_Switch_State;
GestureFile accData, gyroData;

clock_t tStart, lastPress = clock();

CommunicationModule^ comm;

map<int, int> cnt;	// gesture traials counter

void acquiRing::CommunicationModule::reset() {
	mbl_mw_logging_clear_entries(board);
	mbl_mw_debug_reset_after_gc(board);
}

void CommunicationModule::retrainGesture(int gestureID) {
	cnt[gestureID] = 0;
}

void save_data(String^ fnameAcc, String^ fnameGyro) {
	char buffer[500];
	Vector<String^>^ acclines = ref new Vector<String^>();
	Vector<String^>^ gyrolines = ref new Vector<String^>();

	int c = accData.size();
	acclines->Append("X, Y, Z, EPOCH");
	for (int i = 0; i < c; i++) {
		sprintf_s(buffer, "%f, %f, %f, %lld",
			accData[i].second[0], accData[i].second[1], accData[i].second[2], accData[i].first);
		acclines->Append(ref new String(wstring(buffer, buffer + strlen(buffer)).c_str()));
	}
	accData.clear();

	c = gyroData.size();
	gyrolines->Append("X, Y, Z, EPOCH");
	for (int i = 0; i < c; i++) {
		sprintf_s(buffer, "%f, %f, %f, %lld",
			gyroData[i].second[0], gyroData[i].second[1], gyroData[i].second[2], gyroData[i].first);
		gyrolines->Append(ref new String(wstring(buffer, buffer + strlen(buffer)).c_str()));
	}
	gyroData.clear();

	StorageFolder^ storageFolder = Windows::Storage::ApplicationData::Current->LocalFolder;

	if ((int)acclines->Size > DATA_SIZE_MIN) {
		create_task(storageFolder->CreateFileAsync(fnameAcc, CreationCollisionOption::ReplaceExisting))
			.then([acclines](StorageFile^ sampleFile) {
			FileIO::AppendLinesAsync(sampleFile, acclines);
			if (IsDebuggerPresent()) {
				MainPage::writeConsole(sampleFile->Path);
			}
		});
	}

	if ((int)gyrolines->Size > DATA_SIZE_MIN) {
		create_task(storageFolder->CreateFileAsync(fnameGyro, CreationCollisionOption::ReplaceExisting))
			.then([gyrolines](StorageFile^ sampleFile) {
			FileIO::AppendLinesAsync(sampleFile, gyrolines);
			if (IsDebuggerPresent()) {
				MainPage::writeConsole(sampleFile->Path);
			}
		});
	}
}

void data_handler() {
	// handler for accData and gyroData vectors after releasing the push button
	char buffer[500];

	if (IsDebuggerPresent()) {
		MainPage::writeConsole("Acc. Data Size: " + accData.size());
		MainPage::writeConsole("Gyro. Data Size:" + gyroData.size());
		OutputDebugString(L"data_handler\n");
	}

	int trainedGesture = CommunicationModule::Instance->getMode();
	if (trainedGesture > -1) {	// if training mode
		if (trainedGesture > 5) { // if gesture is not standard, check its validity
			auto symbol = ClassifierAdapter::getInstance()->BuildSymbol(trainedGesture, accData, gyroData);
			if (ClassifierAdapter::getInstance()->acceptableGesture(symbol) == false) { // gesture not valid
				TrainPage::NotifyTrainCompletion(-1);
				return;
			}
			//if (ClassifierAdapter::getInstance()->similarityCheck(symbol) == false) {
			//	TrainPage::NotifyTrainCompletion(-2);
			//	return;
			//}
		}

		cnt[trainedGesture]++;
		if (cnt[trainedGesture] > ClassifierAdapter::getInstance()->getTrials()) { // retrain if #trials exceeded
			cnt[trainedGesture] = 1;
		}

		save_data("accOut_" + trainedGesture + "_" + cnt[trainedGesture] + ".txt",
			"gyroOut_" + trainedGesture + "_" + cnt[trainedGesture] + ".txt");
		
		TrainPage::NotifyTrainCompletion(cnt[trainedGesture]);
		if (trainedGesture > 5) {	// if gesture is new, save it.
			Gesture::GestureList->GetAt(Gesture::GestureList->Size - 1)->Undelete();
			Gesture::Save();
		}
	} else {	// else detection mode
		int gID = -1;
		if (IsDebuggerPresent()) {
			tStart = clock();
			gID = ClassifierAdapter::getInstance()->GetGestureID(accData, gyroData);
			sprintf_s(buffer, "GestureID: %d, Time taken: %.3fs\n", gID, (double)(clock() - tStart) / CLOCKS_PER_SEC);
			String^ newLine = ref new String(wstring(buffer, buffer + strlen(buffer)).c_str());
			MainPage::writeConsole(newLine);
			OutputDebugString(newLine->Data());
		} else {
			gID = ClassifierAdapter::getInstance()->GetGestureID(accData, gyroData);
		}

		if (GAME_MODE) {
			//save_data("acc.txt", "gyro.txt");
		}
		save_data("acc.txt", "gyro.txt");

		accData.clear();
		gyroData.clear();
		MainPage::NotifyGestureDetected(gID);
	}

}


/********************************/
/*	Initializers				*/
/********************************/

MblMwDataSignal *batterySignal;
void battery_handler(const MblMwData* data);

void handle_disconnect(MblMwMetaWearBoard* board) {
	static auto cmds_recorded = [](void) -> void {
		//printf("commands recorded\n");
		char buffer[200];
		sprintf_s(buffer, "commands recorded\n");
		String^ newLin = ref new String(wstring(buffer, buffer + strlen(buffer)).c_str());
		MainPage::writeConsole(newLin);

	};

	MblMwLedPattern pattern;
	mbl_mw_led_load_preset_pattern(&pattern, MBL_MW_LED_PRESET_BLINK);
	pattern.repeat_count = 10;

	MblMwEvent* dc_event = mbl_mw_settings_get_disconnect_event(board);
	mbl_mw_event_record_commands(dc_event);
	
	mbl_mw_led_write_pattern(board, &pattern, MblMwLedColor::MBL_MW_LED_COLOR_RED);
	mbl_mw_led_play(board);
	mbl_mw_acc_stop(board);

	mbl_mw_event_end_record(dc_event, cmds_recorded);

}

void initialized() {
	mbl_mw_debug_reset_after_gc(board);

	CommunicationModule::Instance->toggleGPIO(true);

	CommunicationModule::Instance->setMode(-1); // detection mode

	MainPage::NotifyConnectionCompletion();

	handle_disconnect(board);

	batterySignal = mbl_mw_settings_get_battery_state_data_signal(board);
	mbl_mw_datasignal_subscribe(batterySignal, battery_handler);
	//mbl_mw_settings_read_battery_state(board);
}

void write_gatt_char(const MblMwGattChar *characteristic, const uint8_t *value, uint8_t length) {
	// Code to write the value to the characteristic goes here	
	//if (IsDebuggerPresent()) {
	//	MainPage::writeConsole("CommunicationModule::write_gatt_char");
	//}

	Guid guid = HighLow2Uuid(characteristic->uuid_high, characteristic->uuid_low);
	GattCharacteristic^ mwCommandChar = mwGattService->GetCharacteristics(guid)->First()->Current;

	DataWriter^ writer = ref new DataWriter();
	Array<byte>^ managedArray = ref new Array<byte>((unsigned char*)value, unsigned int(length));
	writer->WriteBytes(managedArray);

	create_task(mwCommandChar->WriteValueAsync(writer->DetachBuffer(), GattWriteOption::WriteWithoutResponse))
		.then([](GattCommunicationStatus status) {
		if (status != GattCommunicationStatus::Success) {
			// error
		}
	});
}

void read_gatt_char(const MblMwGattChar *characteristic) {
	// Code to read the value from the characteristic goes here
	if (IsDebuggerPresent()) {
		MainPage::writeConsole("CommunicationModule::read_gatt_char");
	}
}

void received_progress_update(uint32_t entries_left, uint32_t total_entries) {
	char buffer[200];
	sprintf_s(buffer, "Download Progress: %d/%d, Time taken: %.3fs\n",
		entries_left, total_entries, (double)(clock() - tStart) / CLOCKS_PER_SEC);

	if (entries_left == 0 && accData.size() > 0) { // if download completed
		if (IsDebuggerPresent()) {
			String^ newLine = ref new String(wstring(buffer, buffer + strlen(buffer)).c_str());
			MainPage::writeConsole(newLine);
			OutputDebugString(newLine->Data());
		}
		mbl_mw_logging_clear_entries(board);
		mbl_mw_debug_reset_after_gc(board);
		data_handler();
	}
}

void received_unknown_entry(uint8_t id, int64_t epoch, const uint8_t* data, uint8_t length) {
	if (IsDebuggerPresent()) {
		MainPage::writeConsole("CommunicationModule::received_unknown_entry");
	}

	mbl_mw_logging_clear_entries(board);
	mbl_mw_debug_reset_after_gc(board);
	//mbl_mw_debug_reset(board);
}

CommunicationModule::CommunicationModule() {

	comm = this;
	trainedGesture = -1;

	DEVICE_INFO_NAMES.Insert(CHARACTERISTIC_MANUFACTURER, "Manufacturer");
	DEVICE_INFO_NAMES.Insert(CHARACTERISTIC_MODEL_NUMBER, "Model Number");
	DEVICE_INFO_NAMES.Insert(CHARACTERISTIC_SERIAL_NUMBER, "Serial Number");
	DEVICE_INFO_NAMES.Insert(CHARACTERISTIC_FIRMWARE_REVISION, "Firmware Revision");
	DEVICE_INFO_NAMES.Insert(CHARACTERISTIC_HARDWARE_REVISION, "Hardware Revision");

	ClassifierAdapter::getInstance()->TrainGestures();

}

void CommunicationModule::Connect(BluetoothLEDevice^ selectedBtleDevice) {
	btle_conn = { write_gatt_char, read_gatt_char };
	log_down = { received_progress_update, received_unknown_entry };
	board = mbl_mw_metawearboard_create(&btle_conn);
	mbl_mw_metawearboard_initialize(board, initialized);

	mwGattService = selectedBtleDevice->GetGattService(GUID_METAWEAR_SERVICE);

	auto chars = selectedBtleDevice->GetGattService(GUID_DEVICE_INFO_SERVICE)->GetAllCharacteristics();
	for each(GattCharacteristic^ ch in chars) {
		create_task(ch->ReadValueAsync()).then([this, ch](GattReadResult^ result) {
			DataReader^ reader = DataReader::FromBuffer(result->Value);
			byte* data = new byte[result->Value->Length];
			reader->ReadBytes(ArrayReference<unsigned char>(data, result->Value->Length));

			String^ value = result->Status == GattCommunicationStatus::Success ?
				ref new String(wstring(data, data + result->Value->Length).c_str()) : "N/A";
			mwDeviceInfoChars.Insert(ch->Uuid, value);
			//outputListView->Items->Append(DEVICE_INFO_NAMES.Lookup(ch->Uuid) + ": " + value);

			MblMwGattChar* mmgc = new MblMwGattChar();
			Uuid2HighLow(ch->Uuid, mmgc->uuid_high, mmgc->uuid_low);
			mbl_mw_connection_char_read(board, mmgc, data, result->Value->Length);
		});
	}

	mwNotifyChar = mwGattService->GetCharacteristics(METAWEAR_NOTIFY_CHARACTERISTIC)->First()->Current;

	create_task(mwNotifyChar->WriteClientCharacteristicConfigurationDescriptorAsync(
		GattClientCharacteristicConfigurationDescriptorValue::Notify))
		.then([](GattCommunicationStatus status) {
		if (status != GattCommunicationStatus::Success) {
			// error
		}
	});

	mwNotifyChar->ValueChanged += ref new TypedEventHandler<GattCharacteristic^, GattValueChangedEventArgs^>(
		[](GattCharacteristic^ sender, GattValueChangedEventArgs^ obj) {

		DataReader^ reader = DataReader::FromBuffer(obj->CharacteristicValue);
		byte* data = new byte[obj->CharacteristicValue->Length];
		reader->ReadBytes(ArrayReference<unsigned char>(data, obj->CharacteristicValue->Length));
		mbl_mw_connection_notify_char_changed(board, data, obj->CharacteristicValue->Length);
	});

}

void acquiRing::CommunicationModule::Disconnect(bool tear = false) {
	if (tear) {
		mbl_mw_metawearboard_tear_down(board);
		mbl_mw_debug_disconnect(board);
	}else
		mbl_mw_metawearboard_free(board);
}


/********************************/
/*	Sensors Handlers			*/
/********************************/
void acc_recieved_log(const MblMwData* data) {
	MblMwCartesianFloat acc;
	acc.x = ((MblMwCartesianFloat*)data->value)->x;
	acc.y = ((MblMwCartesianFloat*)data->value)->y;
	acc.z = ((MblMwCartesianFloat*)data->value)->z;

	accData.push_back(make_pair(data->epoch, vector<float>{ acc.x, acc.y, acc.z }));
}

void gyro_recieved_log(const MblMwData* data) {
	MblMwCartesianFloat gyro;
	gyro.x = ((MblMwCartesianFloat*)data->value)->x;
	gyro.y = ((MblMwCartesianFloat*)data->value)->y;
	gyro.z = ((MblMwCartesianFloat*)data->value)->z;

	gyroData.push_back(make_pair(data->epoch, vector<float>{ gyro.x, gyro.y, gyro.z }));
}

void acc_logger_ready() { /*writeConsole("Acc. Logger Ready."); */ }
void gyro_logger_ready() { /*writeConsole("Gyro. Logger Ready."); */ }

void sensors_handler(const MblMwData* data, Signal signal) {
	char buffer[200];

	if ((int)accData.size() > DATA_SIZE_MAX || (int)gyroData.size() > DATA_SIZE_MAX) { // force end
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([](void) {
			comm->toggleSensors(false);
		}));
		GPIO_Switch_State = 0;
		MainPage::writeConsole("FORCE END GESTURE - TOO MUCH DATA!\n------------------------------\n");
	}

	if (data->value != NULL) {
		switch (signal) {
		case Signal::ACCELEROMETER:
			if (sensor_enable) {
				MblMwCartesianFloat acc;
				acc.x = ((MblMwCartesianFloat*)data->value)->x;
				acc.y = ((MblMwCartesianFloat*)data->value)->y;
				acc.z = ((MblMwCartesianFloat*)data->value)->z;
				accData.push_back(make_pair(data->epoch, vector<float>{ acc.x, acc.y, acc.z }));
				return;
			}
			return;

			//sprintf_s(buffer, "Acceleration: %.3f, %.3f, %.3f,  %lld\n",
			//	((MblMwCartesianFloat*)data->value)->x,
			//	((MblMwCartesianFloat*)data->value)->y,
			//	((MblMwCartesianFloat*)data->value)->z, data->epoch);

			//break;

		case Signal::GYRO:
			if (sensor_enable) {
				MblMwCartesianFloat gyro;
				gyro.x = ((MblMwCartesianFloat*)data->value)->x;
				gyro.y = ((MblMwCartesianFloat*)data->value)->y;
				gyro.z = ((MblMwCartesianFloat*)data->value)->z;
				gyroData.push_back(make_pair(data->epoch, vector<float>{ gyro.x, gyro.y, gyro.z }));
				return;
			}
			return;

			//sprintf_s(buffer, "Rotation: %.3f, %.3f, %.3f,  %lld\n",
			//	((MblMwCartesianFloat*)data->value)->x,
			//	((MblMwCartesianFloat*)data->value)->y,
			//	((MblMwCartesianFloat*)data->value)->z, data->value);

			//break;

		case Signal::BATTERY:
			sprintf_s(buffer, "Battery State: {Charge: %d%%, Voltage: %d mV}",
				((MblMwBatteryState*)data->value)->charge, ((MblMwBatteryState*)data->value)->voltage);
			mbl_mw_datasignal_unsubscribe(batterySignal);
			break;
		default:
			sprintf_s(buffer, "Unexpected signal data");
			break;
		}

		if (IsDebuggerPresent()) {
			String^ newLine = ref new String(wstring(buffer, buffer + strlen(buffer)).c_str());
			MainPage::writeConsole(newLine);
		}
	}
}
void acc_handler(const MblMwData* data) { sensors_handler(data, Signal::ACCELEROMETER); }
void gyro_handler(const MblMwData* data) { sensors_handler(data, Signal::GYRO); }
void battery_handler(const MblMwData* data) { 
	//sensors_handler(data, Signal::BATTERY); 
	if (GPIO_Switch_State == 1) {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([](void) {
			comm->toggleSensors(false);
		}));
		GPIO_Switch_State = 0;
		MainPage::writeConsole("Log_FORCE END GESTURE - TOO MUCH DATA!\n------------------------------\n");
	}
}

// Not Finished Routine.
int gp_cnt = 0;
int gpio[10];
void gpio_handler_counter(const MblMwData* data) {
	gpio[gp_cnt++] = *(UINT32*)data->value;

	Sleep(10);
	if (gp_cnt > 3) {
		int ones = 0;
		for (int i = 0; i < 3; i++) {
			if (gpio[i] == 1)
				ones++;
		}

		if (ones > 1) {
			accData.clear();
			gyroData.clear();
			CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
				ref new DispatchedHandler([](void) {
				comm->toggleSensors(true);
			}));
		} else {
			CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
				ref new DispatchedHandler([](void) {
				comm->toggleSensors(false);
			}));
		}
	}
}

void gpio_handler(const MblMwData* data) {
	//MblMwDataSignal *gpMsignal = mbl_mw_gpio_get_digital_input_data_signal(board, 0);
	//mbl_mw_datasignal_subscribe(gpMsignal, gpio_handler_counter);
	//mbl_mw_gpio_read_digital_input(board, 0);
	//mbl_mw_gpio_read_digital_input(board, 0);
	//mbl_mw_gpio_read_digital_input(board, 0);
	//mbl_mw_datasignal_unsubscribe(gpMsignal);

	char buffer[200];
	
	double lastPressTime = (double)(clock() - lastPress);
	if (IsDebuggerPresent()) {
		sprintf_s(buffer, "[TRACE] GPIO: %d, GPIO_Switch_State: %d, lastPressTime: %.6f\n", *(UINT32*)data->value, GPIO_Switch_State, lastPressTime);
		String^ newLin = ref new String(wstring(buffer, buffer + strlen(buffer)).c_str());
		MainPage::writeConsole(newLin);
	}
	if (lastPressTime < 50) return;

	sprintf_s(buffer, "GPIO: %d, Last Press: %.3f\n", *(UINT32*)data->value, (double)(clock() - lastPress) / CLOCKS_PER_SEC);
	
	if ((int)*(UINT32*)data->value != GPIO_Switch_State) {	// if there is change in state
		if (GPIO_Switch_State == 0) {	// button pressed
			accData.clear();
			gyroData.clear();
			CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
				ref new DispatchedHandler([](void) {
				comm->toggleSensors(true);
			}));
		} else {	// button released
			CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
				ref new DispatchedHandler([](void) {
				comm->toggleSensors(false);
			}));
		}

		if (IsDebuggerPresent()) {
			String^ newLine = ref new String(wstring(buffer, buffer + strlen(buffer)).c_str());
			MainPage::writeConsole(newLine);
			if (*(UINT32*)data->value == 0) {
				sprintf_s(buffer, "------------- %d -------------\n", debug_cnt++);
				newLine = ref new String(wstring(buffer, buffer + strlen(buffer)).c_str());
				MainPage::writeConsole(newLine);
			}
		}
		lastPress = clock();
	}
	
	GPIO_Switch_State = (int)*(UINT32*)data->value;
	

}

void commands_recordeds() {
	mbl_mw_timer_start(gpio_read_timer);
}

void timer_createds(MblMwTimer* timer) {
	gpio_read_timer = timer;

	mbl_mw_event_record_commands((MblMwEvent*)timer);
	// Read analog input from pin 0 when a timer event is fired
	//mbl_mw_gpio_read_digital_input(board, 0);
	mbl_mw_settings_read_battery_state(board);
	
	mbl_mw_event_end_record((MblMwEvent*)timer, commands_recordeds);
}

void setup_timers() {
	//mbl_mw_timer_create_indefinite(board, 2500, 0, timer_createds);
	mbl_mw_timer_create(board, 3000, 1, (uint8_t)2000, timer_createds);
}


void CommunicationModule::toggleGPIO(bool sw) {
	byte pin = 0;
	MblMwDataSignal *gpMsignal = mbl_mw_gpio_get_pin_monitor_data_signal(board, pin);
	//MblMwDataSignal *gpMsignal = mbl_mw_gpio_get_digital_input_data_signal(board, pin);
	if (sw) {
		mbl_mw_gpio_set_pin_change_type(board, pin, MblMwGpioPinChangeType::MBL_MW_GPIO_PIN_CHANGE_TYPE_ANY);
		mbl_mw_gpio_set_pull_mode(board, pin, MblMwGpioPullMode::MBL_MW_GPIO_PULL_MODE_DOWN);
		mbl_mw_datasignal_subscribe(gpMsignal, gpio_handler);
		mbl_mw_gpio_start_pin_monitoring(board, pin);
		//setup_timers();
	} else {
		mbl_mw_gpio_stop_pin_monitoring(board, pin);
		mbl_mw_datasignal_unsubscribe(gpMsignal);
		//mbl_mw_timer_stop(gpio_read_timer);
		//mbl_mw_timer_remove(gpio_read_timer);

	}

}

void acquiRing::CommunicationModule::setMode(int sw) {
	trainedGesture = sw;

	if (board != nullptr) {
		if (IsDebuggerPresent()) {
			MainPage::writeConsole("setMode: " + sw);
		}

		if (trainedGesture > -1) { // if training mode
			LOG_STREAM = false;
			mbl_mw_acc_bmi160_set_odr(board, MblMwAccBmi160Odr::MBL_MW_ACC_BMI160_ODR_50HZ);
			mbl_mw_acc_bosch_write_acceleration_config(board);
		} else {	// if detection mode
			LOG_STREAM = true;
			mbl_mw_acc_bmi160_set_odr(board, MblMwAccBmi160Odr::MBL_MW_ACC_BMI160_ODR_100HZ);
			mbl_mw_acc_bosch_write_acceleration_config(board);
		}
	}
}


void CommunicationModule::toggleSensors(bool sw) {
	if (!signals.count(Signal::ACCELEROMETER)) {
		signals[Signal::ACCELEROMETER] = mbl_mw_acc_bosch_get_acceleration_data_signal(board);
		mbl_mw_datasignal_log(signals[Signal::ACCELEROMETER], acc_recieved_log, acc_logger_ready);
		//mbl_mw_acc_bmi160_set_odr(board, MblMwAccBmi160Odr::MBL_MW_ACC_BMI160_ODR_100HZ);
		mbl_mw_acc_bosch_set_range(board, MblMwAccBoschRange::MBL_MW_ACC_BOSCH_FSR_4G);
		mbl_mw_acc_bosch_write_acceleration_config(board);
	}

	if (!signals.count(Signal::GYRO)) {
		signals[Signal::GYRO] = mbl_mw_gyro_bmi160_get_rotation_data_signal(board);
		mbl_mw_datasignal_log(signals[Signal::GYRO], gyro_recieved_log, gyro_logger_ready);
		mbl_mw_gyro_bmi160_set_odr(board, MblMwGyroBmi160Odr::MBL_MW_GYRO_BMI160_ODR_25HZ);
		mbl_mw_gyro_bmi160_set_range(board, MblMwGyroBmi160Range::MBL_MW_GYRO_BMI160_FSR_1000DPS);
		mbl_mw_gyro_bmi160_write_config(board);
	}

	if (LOG_STREAM) {
		if(GAME_MODE) mbl_mw_datasignal_subscribe(signals[Signal::GYRO], gyro_handler);
		else mbl_mw_datasignal_subscribe(signals[Signal::ACCELEROMETER], acc_handler);
	}

	if (sw) {
		//const uint8_t* name = (const uint8_t*)"aqcuRing";
		//mbl_mw_settings_set_device_name(board, name, 8);

		accData.clear();
		gyroData.clear();
		sensor_enable = true;

		TrainPage::NotifyTrainStarted();

		if (!LOG_STREAM) {
			mbl_mw_logging_start(board, true);
		}

		setup_timers();

		if (GAME_MODE) {
			mbl_mw_gyro_bmi160_enable_rotation_sampling(board);
			mbl_mw_gyro_bmi160_start(board);
		} else {
			mbl_mw_acc_bosch_enable_acceleration_sampling(board);
			mbl_mw_acc_bosch_start(board);
		}

		MblMwLedPattern *pattern = new MblMwLedPattern();
		mbl_mw_led_load_preset_pattern(pattern, MblMwLedPreset::MBL_MW_LED_PRESET_SOLID);
		mbl_mw_led_write_pattern(board, pattern, MblMwLedColor::MBL_MW_LED_COLOR_BLUE);
		mbl_mw_led_play(board);

	} else {
		mbl_mw_led_stop_and_clear(board);

		if (GAME_MODE) {
			mbl_mw_gyro_bmi160_stop(board);
			mbl_mw_gyro_bmi160_disable_rotation_sampling(board);
		} else {
			mbl_mw_acc_bosch_stop(board);
			mbl_mw_acc_bosch_disable_acceleration_sampling(board);
		}

		if (LOG_STREAM) {	// if stream mode
			mbl_mw_timer_stop(gpio_read_timer);
			mbl_mw_debug_reset_after_gc(board);
			data_handler();

			if (GAME_MODE) mbl_mw_datasignal_unsubscribe(signals[Signal::GYRO]);
			else mbl_mw_datasignal_unsubscribe(signals[Signal::ACCELEROMETER]);

			mbl_mw_logging_clear_entries(board);
			mbl_mw_debug_reset_after_gc(board);
		} else {	// else logging mode
			tStart = clock();
			mbl_mw_logging_stop(board);
			mbl_mw_logging_download(board, 1, &log_down);
		}

		sensor_enable = false;
	}
}

void acquiRing::CommunicationModule::switchRecievingMode(bool mode) {
	// FALSE = LOG, TRUE = STREAM;
	LOG_STREAM = mode;
}

void acquiRing::CommunicationModule::switchSaveData(bool mode) {
	//SAVE_DATA = mode;
	LOG_STREAM = mode;
	GAME_MODE = mode;
	ClassifierAdapter::getInstance()->Game_Mode = mode;
	ClassifierAdapter::getInstance()->TrainGestures();
}