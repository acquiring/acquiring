#pragma once

#include "pch.h"
#include "Plugin.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;

namespace acquiRing {
	public ref class Gesture sealed {
	public:
		Gesture();

		Gesture(int id, String^ name);

		property String^ Name {
			void set(String^ name) { m_name = name; }
			String^ get() { return m_name; }
		}

		property Plugin^ Operation {
			void set(Plugin^ name) { m_operation = name; }
			Plugin^ get() { return m_operation; }
		}

		property int ID {
			void set(int name) { m_ID = name; }
			int get() { return m_ID; }
		}

		property bool Deleted {
			void set(bool name) { m_Deleted = name; }
			bool get() { return m_Deleted; }
		}

		property static IVector<Gesture^>^ GestureList {
			IVector<Gesture^>^ get() { return m_gestureList; }
		}


		void Delete();

		void Undelete() {
			this->Deleted = false;
		}

		static int Load();
		static void Save();

		static Gesture^ getGestureByID(int gID);
	private:
		String^ m_name;
		acquiRing::Plugin^ m_operation;
		int m_ID;
		bool m_Deleted;

		static Vector<Gesture^>^ m_gestureList;
		
	};

}