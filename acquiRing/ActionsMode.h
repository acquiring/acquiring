#pragma once

#include "pch.h"
#include "Gesture.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;

namespace acquiRing {
	public ref class ActionsMode sealed {

	private:
		String^ m_name;
		int m_ID;
		bool m_Deleted;
		Vector<Gesture^>^ m_gestureList;
		static Vector<ActionsMode^>^ m_modeList;
	public:
		ActionsMode();

		ActionsMode(int id, String^ name);

		property String^ Name {
			void set(String^ name) { m_name = name; }
			String^ get() { return m_name; }
		}

		property int ID {
			void set(int name) { m_ID = name; }
			int get() { return m_ID; }
		}

		property bool Deleted {
			void set(bool name) { m_Deleted = name; }
			bool get() { return m_Deleted; }
		}

		property IVector<Gesture^>^ GestureList {
			IVector<Gesture^>^ get() { return m_gestureList; }
		}

		Gesture^ getGestureByID(int gID);

		void SetGestureList(IVector<Gesture^>^ gList) {
			//this->m_gestureList = dynamic_cast<Vector<Gesture^>^>(gList);
			this->m_gestureList->Clear();
			for each (auto gesture in gList) {
				auto g = ref new Gesture(gesture->ID, "");
				if(gesture->Operation != nullptr)
					g->Operation = ref new Plugin(gesture->Operation->UID);
				this->m_gestureList->Append(g);

			}
			//ActionsMode::Save();
		}

		void Delete();

		void Undelete() {
			this->Deleted = false;
		}

		property static IVector<ActionsMode^>^ ModeList {
			IVector<ActionsMode^>^ get() { return m_modeList; }
		}

		static ActionsMode^ getActionModeByID(int amID);

		static int Load();
		static void Save();
	};

}