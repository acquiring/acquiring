﻿#pragma once

#include "pch.h"
#include "GUI/MainPage.g.h"
#include "CommunicationModule.h"
#include "DS.h"
#include "PluginManager.h"
#include <map>
#include "Gesture.h"

using namespace concurrency;
using namespace Platform;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::Storage;
using namespace Windows::Devices::Bluetooth::Advertisement;
using namespace concurrency;
using namespace Windows::UI::Xaml::Media::Imaging;


namespace acquiRing {
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class MainPage sealed {
	public:
		MainPage();

		void static NotifyConnectionCompletion();

		void static NotifyGestureDetected(int gestureID);

		void static writeConsole(String^ msg);

		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;

		void GetPairedBluetoothDevices();

		void updateOutputListView(String^ str);

		void static UpdateGestureListView();
		void updateGestureListView();

		void static UpdateStoreView();
		void updateStoreItems();

		void static UpdateModeListView();
		void updateModeListView();

		bool HM_updateLights(bool on);
		bool HM_updateTV(bool on);
		bool HM_updateAC(bool on);
	private:

		std::map<int, pair<string, string> > gestureMap;

		BluetoothLEAdvertisementWatcher^ watcher;

		void refreshDevices(Object^ sender, RoutedEventArgs^ e);

		void SelectedBtleDevice(Object^ sender, SelectionChangedEventArgs^ e);

		void resetConnectionView();

		void OnAdvertisementReceived(BluetoothLEAdvertisementWatcher^ watcher, BluetoothLEAdvertisementReceivedEventArgs^ eventArgs);

		void showMessageBox(String^ str);

		int getPluginIndex(String^ UID);

		void comboBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);
		void button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		void pluginClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void deleteClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void Store_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnClear_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void swRecievingMode_Toggled(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnDisconnect_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnReset_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void OnConnectionStatusChanged(Windows::Devices::Bluetooth::BluetoothLEDevice ^sender, Platform::Object ^args);
		void swSaveData_Toggled(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnTrainNew_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void ListViewDisplay_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);
		void btnEditSave_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void cbMode_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);
		void btnAddMode_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnEditMode_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnDeleteMode_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnOpenStore_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
	};
}
