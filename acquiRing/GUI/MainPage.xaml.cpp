﻿#include "MainPage.xaml.h"
#include "TrainPage.xaml.h"
#include "Classifier\ClassifierAdapter.h"
#include "Util.h"
#include "ActionsMode.h"
#include <stdio.h>


using namespace acquiRing;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Devices::Enumeration;
using namespace Windows::Devices::Bluetooth;
using namespace Windows::Devices::Bluetooth::GenericAttributeProfile;
using namespace Windows::UI::Core;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::UI::Popups;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::Data::Json;

MainPage^ page;
int editedModeId = -1;
int trainee = -1;

MainPage::MainPage() {
	InitializeComponent();

	this->NavigationCacheMode = Windows::UI::Xaml::Navigation::NavigationCacheMode::Enabled;

	page = this;

	if (!IsDebuggerPresent()) {
		this->acquiRing->Items->RemoveAt(this->acquiRing->Items->Size - 1);		// DebugConsole Tab
		//this->acquiRing->Items->RemoveAt(this->acquiRing->Items->Size - 1);	// Home Automation Tab
	}

	watcher = ref new BluetoothLEAdvertisementWatcher();
	watcher->Received += ref new TypedEventHandler<BluetoothLEAdvertisementWatcher^, BluetoothLEAdvertisementReceivedEventArgs^ >(this, &MainPage::OnAdvertisementReceived);
	watcher->Start();

	Gesture::Load();
	ActionsMode::Load();
	PluginManager::Instance->getPlugins();
}

void MainPage::showMessageBox(String^ str) {
	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		MessageDialog^ msgDialog = ref new MessageDialog(str, "acquiRing");
		page->ConnectionIndicator->IsActive = false;
		create_task(msgDialog->ShowAsync());
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([str](void) {
			page->ConnectionIndicator->IsActive = false;
			MessageDialog^ msgDialog = ref new MessageDialog(str, "acquiRing");
			create_task(msgDialog->ShowAsync());
		}));
	}
}

int acquiRing::MainPage::getPluginIndex(String^ UID) {
	int cnt = 0;
	for each (Platform::Object^ cbItem in comboBox0->Items) {
		auto item = dynamic_cast<ComboBoxItem^>(cbItem);
		if (item == nullptr) continue;

		auto plugin = dynamic_cast<Plugin^>(item->Tag);
		if (plugin != nullptr && UID == plugin->UID) {
			return cnt;
		}
		cnt++;
	}
	return -1;
}

void MainPage::writeConsole(String^ msg) {
	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		page->updateOutputListView(msg);
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([msg](void) {
			page->updateOutputListView(msg);
		}));
	}
}

void acquiRing::MainPage::updateOutputListView(String ^ str) {
	if (this == nullptr) return;
	this->outputListViews->Items->Append(str);
	if (this->outputListViews->Items->Size - 1 < 0)
		return;
	this->outputListViews->SelectedIndex = this->outputListViews->Items->Size - 1;
	this->outputListViews->UpdateLayout();
	this->outputListViews->ScrollIntoView(this->outputListViews->SelectedItem);

	//this->outputListView->ScrollIntoView(this->outputListView->Items[this->outputListView->Items->Size - 1], Windows::UI::Xaml::Controls::ScrollIntoViewAlignment::Leading);
}

void acquiRing::MainPage::UpdateGestureListView() {
	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		page->updateGestureListView();
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([](void) {
			page->updateGestureListView();
		}));
	}
}
void acquiRing::MainPage::updateGestureListView() {
	if (this == nullptr) return;

	this->ListViewDisplay->Items->Clear();
	for each (Gesture^ var in Gesture::GestureList) {
		if (var->Deleted == false && var->ID > 5) {
			this->ListViewDisplay->Items->Append(var);

			if (var->Operation == nullptr) continue;
			auto plugin = PluginManager::Instance->getPluginByID(var->Operation->UID);

			if (plugin == nullptr) continue;
			this->gestureMap[var->ID] = make_pair(ps2ss(plugin->ID), ps2ss(plugin->Extension));
		}
	}
}

void MainPage::UpdateStoreView() {
	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		page->updateStoreItems();
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([](void) {
			page->updateStoreItems();
		}));
	}
}

void acquiRing::MainPage::updateStoreItems() {
	//this->StoreItems->Items->Clear();
	this->comboBox0->Items->Clear();
	this->comboBox1->Items->Clear();
	this->comboBox2->Items->Clear();
	this->comboBox3->Items->Clear();
	this->comboBox4->Items->Clear();
	this->comboBox5->Items->Clear();
	this->cbGestureOp->Items->Clear();
	for each (Plugin^ plugin in PluginManager::Instance->PluginList) {
		//this->StoreItems->Items->Append(plugin);
		ComboBoxItem^ cbitm = ref new ComboBoxItem();
		ComboBoxItem^ cbitm1 = ref new ComboBoxItem();
		ComboBoxItem^ cbitm2 = ref new ComboBoxItem();
		ComboBoxItem^ cbitm3 = ref new ComboBoxItem();
		ComboBoxItem^ cbitm4 = ref new ComboBoxItem();
		ComboBoxItem^ cbitm5 = ref new ComboBoxItem();
		ComboBoxItem^ cbitm6 = ref new ComboBoxItem();

		cbitm->Content = plugin->Name;
		cbitm1->Content = plugin->Name;
		cbitm2->Content = plugin->Name;
		cbitm3->Content = plugin->Name;
		cbitm4->Content = plugin->Name;
		cbitm5->Content = plugin->Name;
		cbitm6->Content = plugin->Name;

		cbitm->Tag = plugin;
		cbitm1->Tag = plugin;
		cbitm2->Tag = plugin;
		cbitm3->Tag = plugin;
		cbitm4->Tag = plugin;
		cbitm5->Tag = plugin;
		cbitm6->Tag = plugin;

		this->comboBox0->Items->Append(cbitm);
		this->comboBox1->Items->Append(cbitm1);
		this->comboBox2->Items->Append(cbitm2);
		this->comboBox3->Items->Append(cbitm3);
		this->comboBox4->Items->Append(cbitm4);
		this->comboBox5->Items->Append(cbitm5);
		this->cbGestureOp->Items->Append(cbitm6);
	}

	int opIdx[7];
	int sz = this->comboBox0->Items->Size;
	for (int i = 0; i < 7; i++) { // 7 combo_boxes
		opIdx[i] = i < sz ? i : sz - 1;
	}

	for each (Gesture^ gesture in Gesture::GestureList) {
		if (gesture->Operation == nullptr) continue;

		auto plugin = PluginManager::Instance->getPluginByID(gesture->Operation->UID);
		if (plugin == nullptr) continue;

		this->gestureMap[gesture->ID] = make_pair(ps2ss(plugin->ID), ps2ss(plugin->Extension));

		int pidx = getPluginIndex(plugin->UID);
		if (pidx != -1) {
			if (gesture->ID < 6) opIdx[gesture->ID] = pidx;
			else if (gesture->ID == trainee) opIdx[6] = pidx;
		}
	}

	this->comboBox0->SelectedIndex = opIdx[0];
	this->comboBox1->SelectedIndex = opIdx[1];
	this->comboBox2->SelectedIndex = opIdx[2];
	this->comboBox3->SelectedIndex = opIdx[3];
	this->comboBox4->SelectedIndex = opIdx[4];
	this->comboBox5->SelectedIndex = opIdx[5];
	this->cbGestureOp->SelectedIndex = opIdx[6];

}

void acquiRing::MainPage::UpdateModeListView() {
	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		page->updateModeListView();
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([](void) {
			page->updateModeListView();
		}));
	}
}

bool isNewMode = false;
void acquiRing::MainPage::updateModeListView() {
	if (this == nullptr) return;

	this->cbMode->Items->Clear();
	int selIdx = -1, cnt = 0;
	for each (auto var in ActionsMode::ModeList) {
		if (var->Deleted == false) {
			ComboBoxItem^ cbitm = ref new ComboBoxItem();
			cbitm->Content = var->Name;
			cbitm->Tag = var;
			this->cbMode->Items->Append(cbitm);
			if (var->ID == editedModeId) {
				selIdx = cnt;
			}
			cnt++;
		}
	}
	if (isNewMode) {
		isNewMode = false;
		this->cbMode->SelectedIndex = this->cbMode->Items->Size - 1;
	} else if (selIdx != -1) {
		this->cbMode->SelectedIndex = selIdx;
	}
}

bool acquiRing::MainPage::HM_updateLights(bool on) {
	if (on) {
		this->imgLights->Source = ref new BitmapImage(ref new Uri("ms-appx:///Assets/bulbon.png"));
		this->txtLights->Text = "Lights ON";
	} else {
		this->imgLights->Source = ref new BitmapImage(ref new Uri("ms-appx:///Assets/bulboff.png"));
		this->txtLights->Text = "Lights OFF";
	}
	return !on;
}

bool acquiRing::MainPage::HM_updateTV(bool on) {
	if (on) {
		this->imgTV->Source = ref new BitmapImage(ref new Uri("ms-appx:///Assets/tvon.png"));
		this->txtTV->Text = "TV ON";
	} else {
		this->imgTV->Source = ref new BitmapImage(ref new Uri("ms-appx:///Assets/tv.png"));
		this->txtTV->Text = "TV OFF";
	}
	return !on;
}

bool acquiRing::MainPage::HM_updateAC(bool on) {
	if (on) {
		this->txtAC->Text = "AC ON";
	} else {
		this->txtAC->Text = "AC OFF";
	}
	return !on;
}

void MainPage::NotifyConnectionCompletion() {
	page->showMessageBox("Connection Successfull!");
}

inline String^ execCode2Str(EXEC_CODE code) {
	return ((int)code).ToString();
}

bool hm1 = true, hm2 = true, hm3 = true;
void Execute(string arg, string extension) {
	if (arg.empty() || extension.empty()) return;

	if (arg == "HM_1") {
		hm1 = page->HM_updateLights(hm1);
		return;
	} else if (arg == "HM_2") {
		hm2 = page->HM_updateTV(hm2);
		return;
	} else if (arg == "HM_3") {
		hm3 = page->HM_updateAC(hm3);
		return;
	}

	auto tStart = clock();
	String^ fileToLaunch = ss2ps("lanucher." + extension);

	StorageFolder^ storageFolder = Windows::Storage::ApplicationData::Current->LocalFolder;
	create_task(storageFolder->CreateFileAsync(fileToLaunch, CreationCollisionOption::ReplaceExisting))
		.then([arg, tStart](StorageFile^ sampleFile) {
		Platform::Collections::Vector<String^>^ args = ref new Platform::Collections::Vector<String^>;
		args->Append(ss2ps(arg));

		Windows::Storage::FileIO::WriteLinesAsync(sampleFile, args);

		auto options = ref new Windows::System::LauncherOptions();
		concurrency::task<bool> launchFileOperation(Windows::System::Launcher::LaunchFileAsync(sampleFile, options));
		launchFileOperation.then([tStart](bool success) {
			if (IsDebuggerPresent()) {
				char buffer[100];
				sprintf_s(buffer, "Execution: %d, Time taken: %.3fs", (int)success, (double)(clock() - tStart) / CLOCKS_PER_SEC);
				MainPage::writeConsole(ref new String(wstring(buffer, buffer + strlen(buffer)).c_str()));
			}
		});
	});
}
void MainPage::NotifyGestureDetected(int gestureID) {
	if (gestureID < 0) return;

	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		auto code = page->gestureMap[gestureID];
		Execute(code.first, code.second);
		page->txtbkNotify->Text = Gesture::getGestureByID(gestureID)->Name;
		page->blinkAnimation->Begin();
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([gestureID](void) {
			auto code = page->gestureMap[gestureID];
			Execute(code.first, code.second);
			page->txtbkNotify->Text = Gesture::getGestureByID(gestureID)->Name;
			page->blinkAnimation->Begin();
		}));
	}
}

void MainPage::OnAdvertisementReceived(BluetoothLEAdvertisementWatcher^ watcher, BluetoothLEAdvertisementReceivedEventArgs^ eventArgs) {
	watcher->Stop();
	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		GetPairedBluetoothDevices();
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([](void) {
			page->GetPairedBluetoothDevices();
		}));
	}

}

void MainPage::OnNavigatedTo(NavigationEventArgs^ e) {
	if (watcher->Status != BluetoothLEAdvertisementWatcherStatus::Started) {
		watcher->Start();
	}
}
int refresh_cnt = 0;

void MainPage::refreshDevices(Object^ sender, RoutedEventArgs^ e) {
	//Execute(EXEC_CODE::MEDIA_PLAYER_PLAY_PAUSE);

	if (refresh_cnt++ > 3 && pairedDevicesListView->Items->Size == 0) {
		refresh_cnt = 0;
		GetPairedBluetoothDevices();
	} else {
		pairedDevicesListView->Items->Clear();
		if (watcher->Status != BluetoothLEAdvertisementWatcherStatus::Started) {
			watcher->Start();
		}
	}
}

void MainPage::GetPairedBluetoothDevices() {
	create_task(DeviceInformation::FindAllAsync(BluetoothLEDevice::GetDeviceSelector()))
		.then([this](DeviceInformationCollection^ deviceCollection) {

		pairedDevicesListView->Items->Clear();
		if (deviceCollection != nullptr) {
			for each(DeviceInformation^ bltInfo in deviceCollection) {
				create_task(BluetoothLEDevice::FromIdAsync(bltInfo->Id))
					.then([this](BluetoothLEDevice^ bleDevice) {
					if (bleDevice != nullptr) {
						pairedDevicesListView->Items->Append(bleDevice);
					}
				});

			}
		}
	});

}

void MainPage::SelectedBtleDevice(Object^ sender, SelectionChangedEventArgs^ e) {
	// Ignore unselection event fire.
	if (e->AddedItems->Size == 0) return;

	//Get the data object that represents the current selected item
	BluetoothLEDevice^ myobject = dynamic_cast<BluetoothLEDevice^>((dynamic_cast<ListView^>(sender))->SelectedItem);

	//Checks whether that it is not null 
	if (myobject != nullptr && myobject->ConnectionStatus == BluetoothConnectionStatus::Connected) {
		auto item = dynamic_cast<ListView^>(sender);
		item->IsEnabled = false;
		this->ConnectionIndicator->IsActive = true;
		// this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(DeviceInfo::typeid), myobject);
		CommunicationModule::Instance->Connect(myobject);
		myobject->ConnectionStatusChanged += ref new Windows::Foundation::TypedEventHandler<Windows::Devices::Bluetooth::BluetoothLEDevice ^, Platform::Object ^>(this, &acquiRing::MainPage::OnConnectionStatusChanged);
	} else {
		GetPairedBluetoothDevices();
	}
}

//if (myobject->DeviceInformation->Pairing->IsPaired == false) {
//	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
//		concurrency::create_task(myobject->DeviceInformation->Pairing->PairAsync()).then(
//			[this, myobject](DevicePairingResult^ pairingResult) {
//			CommunicationModule::Instance->Connect(myobject);
//			myobject->ConnectionStatusChanged += ref new Windows::Foundation::TypedEventHandler<Windows::Devices::Bluetooth::BluetoothLEDevice ^, Platform::Object ^>(this, &acquiRing::MainPage::OnConnectionStatusChanged);
//		}, concurrency::task_continuation_context::use_current());
//	} else {
//		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
//			ref new DispatchedHandler([myobject, this](void) {
//			concurrency::create_task(myobject->DeviceInformation->Pairing->PairAsync()).then(
//				[this, myobject](DevicePairingResult^ pairingResult) {
//				CommunicationModule::Instance->Connect(myobject);
//				myobject->ConnectionStatusChanged += ref new Windows::Foundation::TypedEventHandler<Windows::Devices::Bluetooth::BluetoothLEDevice ^, Platform::Object ^>(this, &acquiRing::MainPage::OnConnectionStatusChanged);
//			}, concurrency::task_continuation_context::use_current());
//		}));
//	}
//} else {

void acquiRing::MainPage::button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	auto btn = dynamic_cast<Button^>(sender);
	int g = ps2ss(btn->Name)[btn->Name->Length() - 1] - '0';
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(TrainPage::typeid), g);
}

String^ makeGestureName(int gID) {
	switch (gID) {
	case 0: return "Right";
	case 1: return "Left";
	case 2: return "Up";
	case 3: return "Down";
	case 4: return "Circle_CW";
	case 5: return "Circle_ACW";
	default: return "Standard";
	}
}

void acquiRing::MainPage::comboBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e) {
	auto cb = dynamic_cast<ComboBox^>(sender);
	if (cb == nullptr) return;

	int g = ps2ss(cb->Name)[cb->Name->Length() - 1] - '0';
	auto item = dynamic_cast<ComboBoxItem^>(cb->SelectedItem);
	if (item == nullptr) return;

	auto plugin = dynamic_cast<Plugin^>(item->Tag);
	if (plugin == nullptr) return;

	this->gestureMap[g] = make_pair(ps2ss(plugin->ID), ps2ss(plugin->Extension));

	bool notFound = true;
	for each (Gesture^ ges in Gesture::GestureList) {
		if (ges->ID == g) {
			ges->Name = makeGestureName(g);
			ges->Operation = plugin;
			notFound = false;
			break;
		}
	}
	if (notFound) {
		auto ges = ref new Gesture(g, makeGestureName(g));
		ges->Undelete();
		ges->Operation = plugin;
		Gesture::GestureList->Append(ges);
	}

	Gesture::Save();
	if (editedModeId != -1) {
		auto mode = ActionsMode::getActionModeByID(editedModeId);
		mode->SetGestureList(Gesture::GestureList);
		ActionsMode::Save();
	}
}

void acquiRing::MainPage::pluginClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	auto btn = dynamic_cast<Button^>(sender);
	auto plugin = dynamic_cast<Plugin^>(btn->DataContext);
	btn->IsEnabled = false;
	plugin->Process();
	btn->IsEnabled = true;
	btn->Content = plugin->isInstalledString;
}

void acquiRing::MainPage::deleteClick(Platform::Object ^ sender, Windows::UI::Xaml::RoutedEventArgs ^ e) {
	auto btn = dynamic_cast<Button^>(sender);
	auto gesture = dynamic_cast<Gesture^>(btn->DataContext);
	gesture->Delete();
	if (editedModeId != -1) {
		auto mode = ActionsMode::ModeList->GetAt(editedModeId);
		mode->SetGestureList(Gesture::GestureList);
	}
}


void acquiRing::MainPage::Store_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {

}


void acquiRing::MainPage::btnClear_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	this->outputListViews->Items->Clear();
	this->outputListViews->UpdateLayout();
}


void acquiRing::MainPage::swRecievingMode_Toggled(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	ToggleSwitch^ toggleSwitch = dynamic_cast<ToggleSwitch^>(sender);
	CommunicationModule::Instance->switchRecievingMode(toggleSwitch->IsOn);
}


void acquiRing::MainPage::btnDisconnect_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	CommunicationModule::Instance->Disconnect(1);
	this->resetConnectionView();
}

void acquiRing::MainPage::btnReset_Click(Platform::Object ^ sender, Windows::UI::Xaml::RoutedEventArgs ^ e) {
	CommunicationModule::Instance->reset();
}

void acquiRing::MainPage::resetConnectionView() {
	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		this->pairedDevicesListView->IsEnabled = true;
		refreshDevices(nullptr, nullptr);
		this->acquiRing->SelectedIndex = 0;
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([this](void) {
			this->pairedDevicesListView->IsEnabled = true;
			refreshDevices(nullptr, nullptr);
			this->acquiRing->SelectedIndex = 0;

		}));
	}
}

void acquiRing::MainPage::OnConnectionStatusChanged(Windows::Devices::Bluetooth::BluetoothLEDevice ^sender, Platform::Object ^args) {
	if (sender->ConnectionStatus == BluetoothConnectionStatus::Disconnected) {
		this->showMessageBox("Disconnected :(");
		CommunicationModule::Instance->Disconnect(0);
		this->resetConnectionView();

	}
}


void acquiRing::MainPage::swSaveData_Toggled(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	ToggleSwitch^ toggleSwitch = dynamic_cast<ToggleSwitch^>(sender);
	CommunicationModule::Instance->switchSaveData(toggleSwitch->IsOn);
}


void acquiRing::MainPage::btnTrainNew_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	int n = 6;
	if (Gesture::GestureList->Size > 0) {
		n = Gesture::GestureList->GetAt(Gesture::GestureList->Size - 1)->ID + 1;
	}

	auto item = dynamic_cast<ComboBoxItem^>(cbGestureOp->SelectedItem);
	if (item == nullptr) return;

	auto plugin = dynamic_cast<Plugin^>(item->Tag);
	if (plugin == nullptr) return;

	this->gestureMap[n] = make_pair(ps2ss(plugin->ID), ps2ss(plugin->Extension));

	Gesture^ newGesture = ref new Gesture(n, this->txtGestureName->Text);
	newGesture->Operation = plugin;

	Gesture::GestureList->Append(newGesture);

	if (editedModeId != -1) {
		auto mode = ActionsMode::ModeList->GetAt(editedModeId);
		mode->SetGestureList(Gesture::GestureList);
	}

	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(TrainPage::typeid), n);
}



void acquiRing::MainPage::ListViewDisplay_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e) {
	// Ignore unselection event fire.
	if (e->AddedItems->Size == 0) {
		this->btnEditSave->IsEnabled = false;
		return;
	}

	this->btnEditSave->IsEnabled = true;

	auto gesture = dynamic_cast<Gesture^>((dynamic_cast<ListView^>(sender))->SelectedItem);
	if (gesture == nullptr) return;

	this->txtGestureName->Text = gesture->Name;

	if (gesture->Operation != nullptr) {
		for each (Platform::Object^ pluginItm in cbGestureOp->Items) {
			auto item = dynamic_cast<ComboBoxItem^>(pluginItm);
			if (item == nullptr) continue;

			auto plugin = dynamic_cast<Plugin^>(item->Tag);
			if (plugin == nullptr) continue;

			if (plugin->UID == gesture->Operation->UID) {
				this->cbGestureOp->SelectedItem = pluginItm;
			}
		}
	}
	trainee = gesture->ID;
}


void acquiRing::MainPage::btnEditSave_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	auto item = dynamic_cast<ComboBoxItem^>(cbGestureOp->SelectedItem);
	if (item == nullptr) return;

	auto plugin = dynamic_cast<Plugin^>(item->Tag);
	if (plugin == nullptr) return;

	this->gestureMap[trainee] = make_pair(ps2ss(plugin->ID), ps2ss(plugin->Extension));

	for each (Gesture^ g in Gesture::GestureList) {
		if (g->ID == trainee) {
			g->Name = txtGestureName->Text;
			g->Operation = plugin;
			break;
		}
	}
	Gesture::Save();
	if (editedModeId != -1) {
		auto mode = ActionsMode::getActionModeByID(editedModeId);
		mode->SetGestureList(Gesture::GestureList);
		ActionsMode::Save();
	}

	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(TrainPage::typeid), trainee);
}

void acquiRing::MainPage::cbMode_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e) {
	if (editedModeId == -1) {
		btnEditMode->IsEnabled = false;
		btnDeleteMode->IsEnabled = false;
	}

	auto cbitem = dynamic_cast<ComboBoxItem^>(this->cbMode->SelectedItem);
	if (cbitem == nullptr) return;

	auto mode = dynamic_cast<ActionsMode^>(cbitem->Tag);
	if (mode == nullptr) return;

	editedModeId = mode->ID;
	if (editedModeId != -1) {
		btnEditMode->IsEnabled = true;
		btnDeleteMode->IsEnabled = true;
	}
	for each (auto gesture in Gesture::GestureList) {
		auto modeGesture = mode->getGestureByID(gesture->ID);
		if (modeGesture == nullptr || modeGesture->Operation == nullptr) continue;

		auto modeGesturePlugin = PluginManager::Instance->getPluginByID(modeGesture->Operation->UID);
		if (modeGesturePlugin == nullptr) continue;

		gesture->Operation = modeGesturePlugin;
	}

	UpdateStoreView();
}

void AddModeClick(String^ name) {
	if (name->IsEmpty()) return;

	int n = 0;
	isNewMode = true;

	if (ActionsMode::ModeList->Size > 0) {
		n = ActionsMode::ModeList->GetAt(ActionsMode::ModeList->Size - 1)->ID + 1;
	}

	auto am = ref new ActionsMode(n, name);
	ActionsMode::ModeList->Append(am);

	ActionsMode::Save();
	page->UpdateModeListView();
}
void acquiRing::MainPage::btnAddMode_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	auto conDlg = ref new ContentDialog();
	conDlg->Title = "Add New Actions Mode";
	conDlg->PrimaryButtonText = "Add";
	conDlg->SecondaryButtonText = "Cancel";

	conDlg->PrimaryButtonClick += ref new TypedEventHandler<ContentDialog^, ContentDialogButtonClickEventArgs^>(
		[](ContentDialog^ sender, ContentDialogButtonClickEventArgs^ obj) {
		auto txtbx = dynamic_cast<TextBox^>(sender->Content);
		AddModeClick(txtbx->Text);
	});


	auto conDlgTxtBox = ref new TextBox();
	conDlgTxtBox->PlaceholderText = "Enter Actions Mode Name ...";
	conDlgTxtBox->KeyUp += ref new KeyEventHandler([conDlg](Platform::Object^ sender, KeyRoutedEventArgs^ e) {
		if (e->Key == Windows::System::VirtualKey::Enter) {
			conDlg->Hide();
			auto txtbx = dynamic_cast<TextBox^>(conDlg->Content);
			AddModeClick(txtbx->Text);
		}
	});

	conDlg->Content = conDlgTxtBox;
	create_task(conDlg->ShowAsync());
}

void EditClick(String^ name) {
	if (name->IsEmpty() || editedModeId == -1) return;
	for each (auto g in ActionsMode::ModeList) {
		if (g->ID == editedModeId) {
			g->Name = name;
			break;
		}
	}
	ActionsMode::Save();
	page->UpdateModeListView();
}

void acquiRing::MainPage::btnEditMode_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	auto conDlg = ref new ContentDialog();
	conDlg->Title = "Edit Actions Mode";
	conDlg->PrimaryButtonText = "Save";
	conDlg->SecondaryButtonText = "Cancel";

	conDlg->PrimaryButtonClick += ref new TypedEventHandler<ContentDialog^, ContentDialogButtonClickEventArgs^>(
		[](ContentDialog^ sender, ContentDialogButtonClickEventArgs^ obj) {
		auto txtbx = dynamic_cast<TextBox^>(sender->Content);
		EditClick(txtbx->Text);
	});


	auto conDlgTxtBox = ref new TextBox();
	conDlgTxtBox->PlaceholderText = "Enter Actions Mode Name ...";
	conDlgTxtBox->KeyUp += ref new KeyEventHandler([conDlg](Platform::Object^ sender, KeyRoutedEventArgs^ e) {
		if (e->Key == Windows::System::VirtualKey::Enter) {
			conDlg->Hide();
			auto txtbx = dynamic_cast<TextBox^>(conDlg->Content);
			EditClick(txtbx->Text);
		}
	});

	conDlg->Content = conDlgTxtBox;

	create_task(conDlg->ShowAsync());
}

void acquiRing::MainPage::btnDeleteMode_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	auto conDlg = ref new Windows::UI::Xaml::Controls::ContentDialog();
	conDlg->Title = "Delete Actions Mode";
	conDlg->PrimaryButtonText = "Yes";
	conDlg->SecondaryButtonText = "No";

	conDlg->PrimaryButtonClick += ref new TypedEventHandler<ContentDialog^, ContentDialogButtonClickEventArgs^>([](ContentDialog^ sender, ContentDialogButtonClickEventArgs^ obj) {
		if (editedModeId > -1) {
			for each (auto g in ActionsMode::ModeList) {
				if (g->ID == editedModeId) {
					g->Delete();
					editedModeId = -1;
					break;
				}
			}
			ActionsMode::Save();
			UpdateModeListView();
		}
	});

	conDlg->Content = "Are you sure that you want to delete this Actions Mode?";
	create_task(conDlg->ShowAsync());
}


void acquiRing::MainPage::btnOpenStore_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	String^ fileToLaunch = ss2ps("lanucher.aquirng_store");

	StorageFolder^ storageFolder = Windows::Storage::ApplicationData::Current->LocalFolder;
	create_task(storageFolder->CreateFileAsync(fileToLaunch, CreationCollisionOption::ReplaceExisting))
		.then([](StorageFile^ sampleFile) {

		auto options = ref new Windows::System::LauncherOptions();
		concurrency::task<bool> launchFileOperation(Windows::System::Launcher::LaunchFileAsync(sampleFile, options));
		launchFileOperation.then([](bool success) {
			int x = 0;
		});
	});
}
