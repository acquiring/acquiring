﻿//
// TrainPage.xaml.h
// Declaration of the TrainPage class
//

#pragma once

#include "GUI/TrainPage.g.h"


using namespace Platform;

namespace acquiRing
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class TrainPage sealed
	{
	public:
		TrainPage();

		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;

		void static NotifyTrainCompletion(int nTrial);
		void static NotifyTrainStarted();
	private:
		int gestureID;

		void button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		void showMessageBox(String^ str);
		void button1_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
	};
}
