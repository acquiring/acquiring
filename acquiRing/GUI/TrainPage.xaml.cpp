﻿//
// TrainPage.xaml.cpp
// Implementation of the TrainPage class
//

#include "pch.h"
#include "TrainPage.xaml.h"
#include "MainPage.xaml.h"
#include "Classifier\ClassifierAdapter.h"
using namespace acquiRing;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Popups;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::UI::Core;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238


TrainPage^ page;

TrainPage::TrainPage() {
	InitializeComponent();

	page = this;
}

void acquiRing::TrainPage::showMessageBox(String^ str) {
	MessageDialog^ msgDialog = ref new MessageDialog(str, "acquiRing");

	//Show message
	create_task(msgDialog->ShowAsync());
}

void acquiRing::TrainPage::NotifyTrainCompletion(int nTrial) {
	String^ msg;
	if (nTrial == -1) {
		msg = "Gesture is very similar to a previously trained gesture, Please try again!";
	} else if (nTrial == -2) {
		msg = "Bad Gesture, Please try again!";
	} else {
		msg = "Success, #Trials: " + nTrial.ToString() + " of " + ClassifierAdapter::getInstance()->getTrials();
	}

	if (CoreApplication::MainView->CoreWindow->Dispatcher->HasThreadAccess) {
		page->LoadingIndicator->IsActive = false;
		page->showMessageBox(msg);
	} else {
		CoreApplication::MainView->CoreWindow->Dispatcher->RunAsync(CoreDispatcherPriority::Normal,
			ref new DispatchedHandler([msg](void) {
			page->LoadingIndicator->IsActive = false;
			page->showMessageBox(msg);
		}));
	}
}

void acquiRing::TrainPage::NotifyTrainStarted() {
	if (page) page->LoadingIndicator->IsActive = true;
}
// Back Button
void acquiRing::TrainPage::button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	ClassifierAdapter::getInstance()->TrainGestures();

	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(MainPage::typeid), safe_cast<Platform::Object^>(2));
	CommunicationModule::Instance->setMode(-1);
}

void acquiRing::TrainPage::OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) {
	int selectedGestureID = (int)(e->Parameter);
	this->gestureID = selectedGestureID;
	CommunicationModule::Instance->setMode(selectedGestureID);
}

// Retrain Button
void acquiRing::TrainPage::button1_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
	CommunicationModule::Instance->retrainGesture(this->gestureID);
}
