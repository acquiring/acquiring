#pragma once

#include "pch.h"

using namespace Platform;

namespace acquiRing {
	public ref class Plugin sealed {
	private:
		String ^m_name, ^m_ID, ^m_extension, ^m_UID;
		bool m_isInstalled;

		bool checkInstalled();

		void install();

		void uninstall();	
	
	public:
		property String^ isInstalledString {
			String^ get() {
				return isInstalled ? "Uninstall" : "Download";
			}
		}
		property bool isInstalled {
			bool get() { return m_isInstalled; }
			void set(bool m) { m_isInstalled = m; }
		}

		property String^ Name {
			void set(String^ name) { m_name = name; }
			String^ get() { return m_name; }
		}

		property String^ Extension {
			void set(String^ name) { m_extension = name; }
			String^ get() { return m_extension; }
		}

		property String^ ID {
			void set(String^ name) { m_ID = name; }
			String^ get() { return m_ID; }
		}

		property String^ UID {
			void set(String^ name) { m_UID = name; }
			String^ get() { return m_UID; }
		}

		Plugin();
		Plugin(String^ uid);
		Plugin(String^ uid, String^ name);
		Plugin(String^ name, String^ id, String^ extension);

		

		void Process();


	};

}