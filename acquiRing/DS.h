#pragma once

#ifndef DS_H
#define DS_H

const GUID GUID_METAWEAR_SERVICE = { 0x326A9000, 0x85CB, 0x9195, 0xD9, 0xDD, 0x46, 0x4C, 0xFB, 0xBA, 0xE7, 0x5A };
const GUID GUID_DEVICE_INFO_SERVICE = { 0x0000180a, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb };
const GUID CHARACTERISTIC_MANUFACTURER = { 0x00002a29, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb };
const GUID CHARACTERISTIC_MODEL_NUMBER = { 0x00002a24, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb };
const GUID CHARACTERISTIC_SERIAL_NUMBER = { 0x00002a25, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb };
const GUID CHARACTERISTIC_FIRMWARE_REVISION = { 0x00002a26, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb };
const GUID CHARACTERISTIC_HARDWARE_REVISION = { 0x00002a27, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb };
const GUID METAWEAR_COMMAND_CHARACTERISTIC = { 0x326A9001, 0x85CB, 0x9195, 0xD9, 0xDD, 0x46, 0x4C, 0xFB, 0xBA, 0xE7, 0x5A };
const GUID METAWEAR_NOTIFY_CHARACTERISTIC = { 0x326A9006, 0x85CB, 0x9195, 0xD9, 0xDD, 0x46, 0x4C, 0xFB, 0xBA, 0xE7, 0x5A };

enum EXEC_CODE {
	PPT_NEXT,
	PPT_PREVIOUS,
	PPT_ZOOMIN,
	PPT_ZOOMOUT,
	MEDIA_PLAYER_PLAY_PAUSE,
	MEDIA_PLAYER_STOP,
	MEDIA_PLAYER_PREVIOUS,
	MEDIA_PLAYER_NEXT,
	MEDIA_PLAYER_VOLUMEUP,
	MEDIA_PLAYER_VOLUMEDOEN,
	MEDIA_PLAYER_MUTE,
	KEY,
	MOUSE_LEFT_CLICK,
	MOUSE_RIGHT_CLICK,
	MOUSE_MOVE,
	SHUTDOWN,
	RESTART,
	LOGOFF,
	LOCK,
	SLEEP,
	HIBERNATE,
	SUBWAY_LEFT,
	SUBWAY_RIGHT,
	SUBWAY_UP,
	SUBWAY_DOWN,
};

enum Signal {
	SWITCH,
	ACCELEROMETER,
	BMP280_PRESSURE,
	BMP280_ALTITUDE,
	AMBIENT_LIGHT,
	GYRO,
	GPIO,
	BATTERY
} ;


#endif