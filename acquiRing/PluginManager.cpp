#include "PluginManager.h"
#include "Util.h"
#include "GUI/MainPage.xaml.h"

using namespace acquiRing;
using namespace Windows::Data::Json;
using namespace Windows::Storage;


PluginManager::PluginManager() {
	m_pluginList = ref new Vector<Plugin^>();
	this->RemoteUri = "http://madel0093-001-site1.etempurl.com/plugins.php";	//"http://127.0.0.1:8000/store/Rss.json";
	this->FileName = "Rss.json";
}


void PluginManager::HandleDownloadAsync(DownloadOperation^ download, boolean start) {
	activeDownloads[download->Guid.GetHashCode()] = download;
	if (start) {
		async = download->StartAsync();
	} else {
		async = download->AttachAsync();
	}
}
void acquiRing::PluginManager::updatePluginList() {

}
void PluginManager::RssLoader() {
	Uri^ source = ref new Uri(this->RemoteUri);
	create_task(Windows::Storage::ApplicationData::Current->LocalFolder->CreateFileAsync(this->FileName, CreationCollisionOption::ReplaceExisting))
		.then([this, source](StorageFile^ destinationFile) {
		BackgroundDownloader^ downloader = ref new BackgroundDownloader();
		DownloadOperation^ download = downloader->CreateDownload(source, destinationFile);
		HandleDownloadAsync(download, true);
	});
}
Plugin ^ acquiRing::PluginManager::getPluginByID(String ^ pid) {
	for each (auto plugin in this->PluginList) {
		if (plugin->UID == pid) return plugin;
	}
	return nullptr;
}
void PluginManager::getPlugins() {
	auto sourceFolder = Windows::Storage::KnownFolders::PicturesLibrary;
	auto targetFolder = ApplicationData::Current->LocalFolder;

	String^ newFileName = "ACQUIRING_APIS.json";

	auto copyFileTask = create_task(sourceFolder->CreateFileAsync(newFileName, CreationCollisionOption::OpenIfExists)).then
		([targetFolder, newFileName, this](StorageFile^ sourceFile) -> task<StorageFile^> {

		auto copyFileTask = sourceFile->CopyAsync(targetFolder, newFileName, Windows::Storage::NameCollisionOption::ReplaceExisting);
		return create_task(copyFileTask);
	}).then([this](StorageFile^ copiedFile) {

		auto responseString = ss2ps(LoadTxtFile("ACQUIRING_APIS.json", 1));

		JsonArray^ mapValue = ref new JsonArray();

		if (JsonArray::TryParse(responseString, &mapValue)) {
			auto vec = mapValue->GetView();

			PluginList->Clear();
			//[{"ID":"1", "Name" : "plugin1 ", "Exetnsion" : "acquiring_9"}
			std::for_each(begin(vec), end(vec), [this](IJsonValue^ M) {
				PluginList->Append(
					ref new Plugin(
						M->GetObject()->GetNamedString("Name"),
						M->GetObject()->GetNamedString("ID"),
						M->GetObject()->GetNamedString("Exetnsion")
						)
					);
			});
			PluginList->Append(ref new Plugin("Home Automation: Bulb", "HM_1", "homeRing"));
			PluginList->Append(ref new Plugin("Home Automation: TV", "HM_2", "homeRing"));
			PluginList->Append(ref new Plugin("Home Automation: AC", "HM_3", "homeRing"));

			int cnt = 0;
			for each (Gesture^ gesture in Gesture::GestureList) {
				if (gesture->Operation == nullptr) {
					gesture->Operation = PluginList->GetAt(cnt);
					cnt = cnt + 1 < (int)PluginList->Size ? cnt+1 : cnt;
				}
			}
			MainPage::UpdateStoreView();
		}
	});
}
