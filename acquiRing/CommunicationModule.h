#pragma once

#include "pch.h"
#include "metawear/core/connection.h"
#include "metawear/core/metawearboard.h"
#include <guiddef.h>
#include <unordered_map>
#include <string>

using namespace std;
using namespace Windows::Devices::Bluetooth;
using namespace Windows::Devices::Bluetooth::GenericAttributeProfile;
using namespace Platform;
using namespace Windows::Storage::Streams;
using namespace concurrency;
using namespace Windows::ApplicationModel::Core;

namespace acquiRing {
	ref class CommunicationModule sealed {
		

	private:
		CommunicationModule();

		int trainedGesture;

	public:
		// Singleton
		static property CommunicationModule^ Instance {
			CommunicationModule^ get() {
				static CommunicationModule^ instance = ref new CommunicationModule();
				return instance;
			}
		}

		void Connect(BluetoothLEDevice^ selectedBtleDevice);
		void Disconnect(bool tear);

		void toggleSensors(bool sw);

		void toggleGPIO(bool sw);

		void setMode(int sw);

		int getMode() {
			return trainedGesture;
		}

		void reset();

		void retrainGesture(int gestureID);

		void switchRecievingMode(bool mode);
		void switchSaveData(bool mode);
	};
}

