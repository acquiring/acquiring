#pragma once

#ifndef UTIL_H
#define UTIL_H

#include<string>
#include<sstream>
#include<fstream>
#include"DS.h"

using namespace std;
using namespace Platform;

const int DATA_SIZE_MAX = 300;
const int DATA_SIZE_MIN = 5;

/// <summary>
/// Standard string to Plarform String.
/// </summary>
/// <param name="s">The standard string.</param>
/// <returns>Platform String</returns>
inline String^ ss2ps(string s) {
	return ref new String(wstring(s.begin(), s.end()).c_str());
}

/// <summary>
/// Platform String to standard string
/// </summary>
/// <param name="s">The Platfrom String</param>
/// <returns> Standard string</returns>
inline string ps2ss(String^ s) {
	wstring wpath(s->Begin());
	string ss(wpath.begin(), wpath.end());
	return ss;
}

inline String^ execCode2String(EXEC_CODE code) {
	return ((int)code).ToString();
}

inline void Uuid2HighLow(Guid uuid, uint64_t &high, uint64_t &low) {
	String^ str = uuid.ToString();
	wstring wstr = str->Data();
	string hx(wstr.begin(), wstr.end());

	for (int i = 0; i < (int)hx.size(); i++)
		if (hx[i] == '-' || hx[i] == '{' || hx[i] == '}') hx[i] = ' ';

	istringstream iss(hx);
	string c1, c2, p1, p2, p3, p4, p5;
	iss >> p1 >> p2 >> p3 >> p4 >> p5;
	c1 = p1 + p2 + p3;
	c2 = p4 + p5;
	sscanf_s(c1.c_str(), "%llx", &high);
	sscanf_s(c2.c_str(), "%llx", &low);
}

inline Guid HighLow2Uuid(const uint64_t high, const uint64_t low) {
	GUID guid;
	stringstream sstream;
	sstream << std::hex << high;
	sstream << std::hex << low;
	string s = sstream.str();

	unsigned long p1;
	int p2, p3, p4[8];

	int err = sscanf_s(s.c_str(), "%08lX%04X%04X%02X%02X%02X%02X%02X%02X%02X%02X",
		&p1, &p2, &p3, &p4[0], &p4[1], &p4[2], &p4[3], &p4[4], &p4[5], &p4[6], &p4[7]);

	guid.Data1 = p1;
	guid.Data2 = p2;
	guid.Data3 = p3;
	for (int i = 0; i < 8; i++) guid.Data4[i] = p4[i];

	return guid;
}

inline string LoadTxtFile(Platform::String^ filename, int pickFolder = 0) {
	//auto folder = Windows::ApplicationModel::Package::Current->InstalledLocation;
	auto folder = Windows::Storage::ApplicationData::Current->LocalFolder;

	Platform::Array<uint8>^ data = nullptr;
	std::wstring dir = folder->Path->ToString()->Data();
	
	auto fullpath = dir.append(L"\\").append(filename->Data());

	std::ifstream file(fullpath, std::ios::in | std::ios::ate);
	// if opened read it in
	if (file.is_open()) {
		int length = (int)file.tellg();
		data = ref new Platform::Array<uint8>(length);
		file.seekg(0, std::ios::beg);
		file.read(reinterpret_cast<char*>(data->Data), length);
		file.close();
	} else {
		return "";
	}

	std::wstring output;
	for (int i = 0; i < (int)data->Length; i++)
		output += data[i];

	return string(output.begin(), output.end());
}

inline void SaveTxtFile(Platform::String^ filename, String^ str) {
	auto folder = Windows::Storage::ApplicationData::Current->LocalFolder;

	Platform::Array<uint8>^ data = nullptr;
	std::wstring dir = folder->Path->ToString()->Data();

	auto fullpath = dir.append(L"\\").append(filename->Data());

	std::ofstream file(fullpath, std::ios::out | std::ios::ate);
	if (file.is_open()) {
		string stdstr = ps2ss(str);
		file.write(stdstr.c_str(), stdstr.size());
		file.close();
	}
}

inline int s2i(string s) {
	istringstream iss(s);
	int n;
	iss >> n;
	return n;
}

inline string i2s(int n) {
	char buf[100];
	sprintf_s(buf, "%d", n);
	
	return string(buf, buf + strlen(buf));
}

inline vector<string> split(const string &s, char delim) {
	vector<string> elems;
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

#endif
