﻿#pragma once
//------------------------------------------------------------------------------
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//------------------------------------------------------------------------------

#include "XamlBindingInfo.g.h"

namespace Windows {
    namespace UI {
        namespace Xaml {
            namespace Media {
                namespace Animation {
                    ref class Storyboard;
                }
            }
        }
    }
}
namespace Windows {
    namespace UI {
        namespace Xaml {
            namespace Controls {
                ref class TextBlock;
                ref class Pivot;
                ref class PivotItem;
                ref class ListView;
                ref class Button;
                ref class ToggleSwitch;
                ref class Image;
                ref class TextBox;
                ref class ComboBox;
                ref class ProgressRing;
                ref class CheckBox;
            }
        }
    }
}

namespace acquiRing
{
    [::Windows::Foundation::Metadata::WebHostHidden]
    partial ref class MainPage : public ::Windows::UI::Xaml::Controls::Page, 
        public ::Windows::UI::Xaml::Markup::IComponentConnector,
        public ::Windows::UI::Xaml::Markup::IComponentConnector2
    {
    public:
        void InitializeComponent();
        virtual void Connect(int connectionId, ::Platform::Object^ target);
        virtual ::Windows::UI::Xaml::Markup::IComponentConnector^ GetBindingConnector(int connectionId, ::Platform::Object^ target);
    
    private:
        bool _contentLoaded;
        class MainPage_obj27_Bindings;
        class MainPage_obj60_Bindings;
    
        ::XamlBindingInfo::XamlBindings^ Bindings;
        private: ::Windows::UI::Xaml::Media::Animation::Storyboard^ blinkAnimation;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ txtbkNotify;
        private: ::Windows::UI::Xaml::Controls::Pivot^ acquiRing;
        private: ::Windows::UI::Xaml::Controls::PivotItem^ Connection;
        private: ::Windows::UI::Xaml::Controls::PivotItem^ GestureAssign;
        private: ::Windows::UI::Xaml::Controls::PivotItem^ AddNewGesture;
        private: ::Windows::UI::Xaml::Controls::PivotItem^ Home;
        private: ::Windows::UI::Xaml::Controls::PivotItem^ ConsoleDebug;
        private: ::Windows::UI::Xaml::Controls::ListView^ outputListViews;
        private: ::Windows::UI::Xaml::Controls::Button^ btnClear;
        private: ::Windows::UI::Xaml::Controls::Button^ btnDisconnect;
        private: ::Windows::UI::Xaml::Controls::Button^ btnReset;
        private: ::Windows::UI::Xaml::Controls::ToggleSwitch^ swRecievingMode;
        private: ::Windows::UI::Xaml::Controls::ToggleSwitch^ swGameMode;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ txtLights;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ txtTV;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ txtAC;
        private: ::Windows::UI::Xaml::Controls::Image^ imgLights;
        private: ::Windows::UI::Xaml::Controls::Image^ imgTV;
        private: ::Windows::UI::Xaml::Controls::Image^ imgAC;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ textBlock1;
        private: ::Windows::UI::Xaml::Controls::TextBox^ txtGestureName;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ cbGestureOp;
        private: ::Windows::UI::Xaml::Controls::Button^ btnTrainNew;
        private: ::Windows::UI::Xaml::Controls::Button^ btnEditSave;
        private: ::Windows::UI::Xaml::Controls::ListView^ ListViewDisplay;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ cbMode;
        private: ::Windows::UI::Xaml::Controls::Button^ btnAddMode;
        private: ::Windows::UI::Xaml::Controls::Button^ btnEditMode;
        private: ::Windows::UI::Xaml::Controls::Button^ btnDeleteMode;
        private: ::Windows::UI::Xaml::Controls::Button^ btnOpenStore;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ textBlock;
        private: ::Windows::UI::Xaml::Controls::Image^ image;
        private: ::Windows::UI::Xaml::Controls::Image^ image1;
        private: ::Windows::UI::Xaml::Controls::Image^ image2;
        private: ::Windows::UI::Xaml::Controls::Image^ image3;
        private: ::Windows::UI::Xaml::Controls::Image^ image4;
        private: ::Windows::UI::Xaml::Controls::Image^ image5;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ comboBox0;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ comboBox1;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ comboBox2;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ comboBox3;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ comboBox4;
        private: ::Windows::UI::Xaml::Controls::ComboBox^ comboBox5;
        private: ::Windows::UI::Xaml::Controls::Button^ button0;
        private: ::Windows::UI::Xaml::Controls::Button^ button1;
        private: ::Windows::UI::Xaml::Controls::Button^ button2;
        private: ::Windows::UI::Xaml::Controls::Button^ button3;
        private: ::Windows::UI::Xaml::Controls::Button^ button4;
        private: ::Windows::UI::Xaml::Controls::Button^ button5;
        private: ::Windows::UI::Xaml::Controls::ProgressRing^ ConnectionIndicator;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ textBlock2;
        private: ::Windows::UI::Xaml::Controls::Button^ button;
        private: ::Windows::UI::Xaml::Controls::CheckBox^ checkBox;
        private: ::Windows::UI::Xaml::Controls::ListView^ pairedDevicesListView;
    };
}

