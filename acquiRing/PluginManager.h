#pragma once

#include "pch.h"
#include "Plugin.h"

using namespace std;
using namespace Platform::Collections;
using namespace Platform;
using namespace Windows::Networking::BackgroundTransfer;
using namespace Windows::Web;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage;

namespace acquiRing {
	ref class PluginManager sealed {
	private:
		Vector<Plugin^>^ m_pluginList;
		std::map<int /*Platform::Guid*/, Windows::Networking::BackgroundTransfer::DownloadOperation^> activeDownloads;
		PluginManager();
		String^ RemoteUri;
		String^ FileName;
		IAsyncOperationWithProgress<DownloadOperation^, DownloadOperation^>^ async;
	public:

		// Singleton
		static property PluginManager^ Instance {
			PluginManager^ get() {
				static PluginManager^ instance = ref new PluginManager();
				return instance;
			}
		}

		property IVector<Plugin^> ^PluginList {
			IVector<Plugin^>^ get() {
				return m_pluginList;
			}
		}

		Plugin^ getPluginByID(String^ pid);

		void getPlugins();
		void RssLoader();
		void HandleDownloadAsync(DownloadOperation^ download, boolean start);

		void updatePluginList();
	};

}