#include "ClassifierAdapter.h"
#include <sstream>
#include "GUI/MainPage.xaml.h"
#include "Util.h"
#include "Gesture.h"
using namespace acquiRing;


//gesture and trial base 0
string ClassifierAdapter::getFileName(int gesture, int trial, bool acc) {
	char str[50];
	int j = (gesture * 3) + trial + 1;

	auto path = "";
	if (acc) {
		sprintf_s(str, "gyroOut_%d_%d.txt", gesture, trial + 1);
	} else {
		sprintf_s(str, "accOut_%d_%d.txt", gesture, trial + 1);
	}
	return string(str);
}

GestureFile  ClassifierAdapter::readData(String^ path, int gesture, int trial, bool acc) {
	auto filename = ss2ps(getFileName(gesture, trial, acc));

	if (path->End() != L"\\" && path->End() != L"/")
		path = String::Concat(path, "/");

	path = String::Concat(path, filename);
	auto out = LoadTxtFile(path);
	GestureFile f;

	istringstream iss(out);
	string line;
	float x, y, z;
	long long epoch;
	getline(iss, line, '\n');
	while (getline(iss, line, '\n')) {
		sscanf_s(line.c_str(), "%f, %f, %f, %lld", &x, &y, &z, &epoch);
		f.push_back(make_pair(epoch, vector<float>{ x, y, z }));
	}

	return f;
}

void ClassifierAdapter::movingAvg(vector<vector<float> > &pre, int windowSize) {
	//windows size should be odd
	if (windowSize % 2 == 0)
		return;
	int half = windowSize / 2;
	for (int row = half; row < (int)pre.size() - half; row++) {
		int from = row - half;
		int to = row + half;
		vector<float> temp;
		for (int col = 0; col < (int)pre[row].size(); col++) {
			float sum = 0;
			for (int i = from; i <= to; i++) {
				sum += pre[i][col];
			}
			temp.push_back(sum / windowSize);
		}
		pre[row] = temp;
	}
}
vector<vector<float> > ClassifierAdapter::InterpolateMissing(GestureFile acc) {
	vector<vector<float> > temp;
	if (acc.size() == 0) return temp;

	temp.push_back(acc[0].second);
	for (int i = 1; i < (int)acc.size(); i++) {
		float diff = (float)(acc[i].first - acc[i - 1].first);
		//ignore interpolation
		if (false && diff > T*1.5) { 
			int missing = (int)round(diff / T) - 1;
			vector<float> miss;
			for (int j = 0; j < (int)acc[i].second.size(); j++)
				miss.push_back((float)((acc[i].second[j] + acc[i - 1].second[j]) / 2.0));
			for (int j = 0; j < missing; j++)
				temp.push_back(miss);
		} else
			temp.push_back(acc[i].second);
	}
	//uncomment to activate moving average filter with window size 3
	//movingAvg(temp, 3);
	return temp;
}

ClassifierAdapter::ClassifierAdapter() {
	Trials = 1;
	this->Game_Mode = false;
}

int dataAvgSize = 0;
void ClassifierAdapter::TrainGestures(int parameters, String^ path, int NumOfTrainingSamplesPerGesture, int gesturesNum) {
	classifier.parameters = parameters;
	classifier.TrainiedData.clear();
	//for (int gesture = 0; gesture < 6; gesture++) {
	//	int gAvgSize = 0;
	//	for (int trial = 0; trial < NumOfTrainingSamplesPerGesture; trial++) {
	//		Symbols s;
	//		s.id = gesture;
	//		//s.referenceData.push_back(Merge(readData(path, gesture, trial, true), readData(path, gesture, trial, false)));
	//		s.referenceData.push_back(InterpolateMissing(readData(path, gesture, trial, Game_Mode)));
	//		classifier.TrainiedData.push_back(s);
	//		gAvgSize += (int)s.referenceData.size();
	//	}
	//	dataAvgSize += gAvgSize;
	//}

	gesturesNum = Gesture::GestureList->Size;
	for (int gesture = 0; gesture < gesturesNum; gesture++) {
		Gesture^ gesObj = Gesture::GestureList->GetAt(gesture);
		if (gesObj->Deleted == true) continue;
		
		int gAvgSize = 0;
		for (int trial = 0; trial < NumOfTrainingSamplesPerGesture; trial++) {
			Symbols s;
			s.id = gesObj->ID;
			//s.referenceData.push_back(Merge(readData(path, gesture, trial, true), readData(path, gesture, trial, false)));
			s.referenceData.push_back(InterpolateMissing(readData(path, s.id, trial, Game_Mode)));
			classifier.TrainiedData.push_back(s);
			gAvgSize += (int)s.referenceData.size();
		}
		dataAvgSize += gAvgSize;
	}
	dataAvgSize += dataAvgSize / 2;
}

Symbols ClassifierAdapter::BuildSymbol(int id, GestureFile accData, GestureFile gyroData) {
	Symbols s;
	s.id = id;
	s.referenceData.push_back(InterpolateMissing(accData));
	return s;
}

double ClassifierAdapter::getAccuracy() {
	double total = 0, correct = 0;
	for (auto& it : classifier.TrainiedData) {
		for (auto &it2 : it.referenceData) {
			int id = classifier.Classify(it2).first.first;
			if (id == it.id) correct++; total++;
		}
	}
	return correct / total;
}

bool ClassifierAdapter::similarityCheck(Symbols NewGesture) {
	double BeforeAddingTheSymbol = getAccuracy();
	classifier.TrainiedData.push_back(NewGesture);
	double AfterAddingTheSymbol = getAccuracy();
	double delta = BeforeAddingTheSymbol - AfterAddingTheSymbol;

	if (IsDebuggerPresent()) {
		MainPage::writeConsole("after adding: " + AfterAddingTheSymbol);
		MainPage::writeConsole("Similarity: " + delta);
	}

	return delta < .05;
}

bool ClassifierAdapter::acceptableGesture(Symbols NewGesture) {
	auto result = classifier.Classify(NewGesture.referenceData[0]);
	float dist = result.first.second;
	if (IsDebuggerPresent()) {
		MainPage::writeConsole("checker, id: " + result.first.first + ", distance: " + result.first.second + ", traineeID: " + NewGesture.id);
	}
	return dist > SIM_THRESH || result.first.first == NewGesture.id;
}

int ClassifierAdapter::GetGestureID(GestureFile accData, GestureFile gyroData) {
	if (Game_Mode) {
		if ((int)gyroData.size() <= DATA_SIZE_MIN) return -1;
	} else {
		if ((int)accData.size() <= DATA_SIZE_MIN) return -1;
	}

	if ((int)accData.size() >= DATA_SIZE_MAX || (int)gyroData.size() > DATA_SIZE_MAX) {
		//return -2;
		if (dataAvgSize < (int)accData.size()) {
			accData = GestureFile(accData.begin(), accData.begin() + dataAvgSize);
		}
		if (dataAvgSize < (int)gyroData.size()) {
			gyroData = GestureFile(gyroData.begin(), gyroData.begin() + dataAvgSize);
		}

		if (IsDebuggerPresent()) {
			MainPage::writeConsole("New Acc Size: " + accData.size());
			MainPage::writeConsole("New Gyro Size: " + gyroData.size());
		}
	}

	vector<vector<float> > merged = InterpolateMissing(Game_Mode ? gyroData : accData);
	if (merged.size() <= DATA_SIZE_MIN) return -1;

	auto gesutreClass = classifier.Classify(merged);

	if (IsDebuggerPresent()) {
		MainPage::writeConsole("Merged Size: " + merged.size());
		MainPage::writeConsole("GestureID: " + gesutreClass.first.first + ", Globale Distance: " + gesutreClass.first.second);
		MainPage::writeConsole("Tested with " + gesutreClass.second.first + " gestures");
		//MainPage::writeConsole("StatsGestureID: " + gesutreClass.second.first + ", Stats Globale Distance: " + gesutreClass.second.second);
	}

	//if (gesutreClass.first.second > G_THREHOLD)
	//	return -3;

	return gesutreClass.first.first;
}

void ClassifierAdapter::TrainGestures() {
	int nGestures = Gesture::GestureList->Size;

	TrainGestures(3, "", Trials, nGestures);
}
