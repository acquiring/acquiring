#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "Classifier\Classifier.h"


//#include "Classifier\ClassifierMultiThread.h"
#define FREQ 50
const int T = (int)(1000 * (1.0 / FREQ));

using namespace Platform;

typedef vector< pair<long long, vector<float> > > GestureFile;

class ClassifierAdapter {
private:
	Classifier classifier;
	//ClassifierMultiThread classifier;


	
	GestureFile readData(String^ path, int gesture, int trial, bool acc);
	vector<vector<float> > InterpolateMissing(GestureFile acc);
	void movingAvg(vector<vector<float> > &pre, int windowSize);

	//int G_THREHOLD = 30;
	const float SIM_THRESH = 15;

	ClassifierAdapter();

	int Trials;
public:
	bool Game_Mode;
	static ClassifierAdapter* getInstance() {
		static ClassifierAdapter instance;
		return &instance;
	}

	int getTrials() {
		return Trials;
	}

	string getFileName(int gesture, int trial, bool acc);

	//returns Gesture Id base 0
	int GetGestureID(GestureFile accData, GestureFile gyroData);

	void TrainGestures();

	void TrainGestures(int parameters, String^ path, int NumOfTrainingSamplesPerGesture, int gesturesNum);

	Symbols BuildSymbol(int id, GestureFile accData, GestureFile gyroData);
	
	double getAccuracy();
	bool similarityCheck(Symbols NewGesture);	
	bool acceptableGesture(Symbols NewGesture);
};

