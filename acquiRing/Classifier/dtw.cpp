#include "dtw.h"
float DTW::dtw(vector< vector<float> > x, vector<vector<float> > y, unsigned int params)
{
	if (x.size() < 5 || y.size() < 5) {
		return VERY_BIG;
	}
	double **globdist;
	double **Dist;

	double top, mid, bot, cheapest, total;
	unsigned short int **move;
	unsigned short int **warp;
	unsigned short int **temp;

	unsigned int I, X, Y, n, i, j, k;
	//unsigned int Xratio, Yratio;
	//unsigned int debug; /* debug flag */
	unsigned int xsize, ysize;
	//FILE *file1, *file2, *glob;
	xsize = x.size();
	ysize = y.size();
	/* allocate memory for Dist */

	if ((Dist = (double **)malloc(xsize * sizeof(double *))) == NULL)
		fprintf(stderr, "Memory allocation error (Dist)\n");

	for (i = 0; i < xsize; i++)
	if ((Dist[i] = (double *)malloc(ysize * sizeof(double))) == NULL)
		fprintf(stderr, "Memory allocation error (Dist)\n");

	/* allocate memory for globdist */

	if ((globdist = (double **)malloc(xsize * sizeof(double *))) == NULL)
		fprintf(stderr, "Memory allocation error (globdist)\n");

	for (i = 0; i < xsize; i++)
	if ((globdist[i] = (double *)malloc(ysize * sizeof(double))) == NULL)
		fprintf(stderr, "Memory allocation error (globdist)\n");

	/* allocate memory for move */

	if ((move = (unsigned short **)malloc(xsize * sizeof(short *))) == NULL)
		fprintf(stderr, "Memory allocation error (move)\n");

	for (i = 0; i < xsize; i++)
	if ((move[i] = (unsigned short *)malloc(ysize * sizeof(short))) == NULL)
		fprintf(stderr, "Memory allocation error (move)\n");

	/* allocate memory for temp */

	if ((temp = (unsigned short **)malloc(xsize * 3 * sizeof(short *))) == NULL)
		fprintf(stderr, "Memory allocation error (temp)\n");

	for (i = 0; i < xsize * 3; i++)
	if ((temp[i] = (unsigned short *)malloc(3 * sizeof(short))) == NULL)
		fprintf(stderr, "Memory allocation error (temp)\n");

	/* allocate memory for warp */

	if ((warp = (unsigned short **)malloc(xsize * 3 * sizeof(short *))) == NULL)
		fprintf(stderr, "Memory allocation error (warp)\n");

	for (i = 0; i < xsize * 3; i++)
	if ((warp[i] = (unsigned short *)malloc(3 * sizeof(short))) == NULL)
		fprintf(stderr, "Memory allocation error (warp)\n");
	double volicty = 0;
	/*Compute distance matrix*/

	for (i = 0; i<xsize; i++) {
		for (j = 0; j<ysize; j++) {
			total = 0;
			for (k = 0; k<params; k++) {
				total = total + ((x[i][k] - y[j][k]) * (x[i][k] - y[j][k]));
			}
			Dist[i][j] = total;
		}
	}

	/*% for first frame, only possible match is at (0,0)*/

	globdist[0][0] = Dist[0][0];
	for (j = 1; j<xsize; j++)
		globdist[j][0] = VERY_BIG;

	globdist[0][1] = VERY_BIG;
	globdist[1][1] = globdist[0][0] + Dist[1][1];
	move[1][1] = 2;

	for (j = 2; j<xsize; j++)
		globdist[j][1] = VERY_BIG;

	for (i = 2; i<ysize; i++) {
		globdist[0][i] = VERY_BIG;
		globdist[1][i] = globdist[0][i - 1] + Dist[1][i];

		for (j = 2; j<xsize; j++) {
			top = globdist[j - 1][i - 2] + Dist[j][i - 1] + Dist[j][i];
			mid = globdist[j - 1][i - 1] + Dist[j][i];
			bot = globdist[j - 2][i - 1] + Dist[j - 1][i] + Dist[j][i];
			if ((top < mid) && (top < bot))
			{
				cheapest = top;
				I = 1;
			}
			else if (mid < bot)
			{
				cheapest = mid;
				I = 2;
			}
			else {
				cheapest = bot;
				I = 3;
			}

			/*if all costs are equal, pick middle path*/
			if ((top == mid) && (mid == bot))
				I = 2;

			globdist[j][i] = cheapest;
			move[j][i] = I;
		}
	}



	X = ysize - 1; Y = xsize - 1; n = 0;
	warp[n][0] = X; warp[n][1] = Y;


	while (X > 0 && Y > 0 && n + 1<3 * xsize) {
		n = n + 1;
		if (move[Y][X] == 1)
		{
			warp[n][0] = X - 1; warp[n][1] = Y;
			n = n + 1;
			X = X - 2; Y = Y - 1;
		}
		else if (move[Y][X] == 2)
		{
			X = X - 1; Y = Y - 1;
		}
		else if (move[Y][X] == 3)
		{
			warp[n][0] = X;
			warp[n][1] = Y - 1;
			n = n + 1;
			X = X - 1; Y = Y - 2;
		}
		warp[n][0] = X;
		warp[n][1] = Y;

	}


	/*flip warp*/
	for (i = 0; i <= n; i++) {
		temp[i][0] = warp[n - i][0];
		temp[i][1] = warp[n - i][1];

	}

	for (i = 0; i <= n; i++) {
		warp[i][0] = temp[i][0];
		warp[i][1] = temp[i][1];

	}
	// mem-deallocation
	float ret = (float)globdist[xsize - 1][ysize - 1];
	{
		for (i = 0; i < xsize; i++)free(Dist[i]), free(globdist[i]), free(move[i]);
		for (i = 0; i < xsize * 2; i++) free(temp[i]), free(warp[i]);
		free(Dist);
		free(globdist);
		free(move);
		free(temp);
		free(warp);
	}
	return ret;
}