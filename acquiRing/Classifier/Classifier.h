#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include "dtw.h"

using namespace std;

#ifndef CLASSIFIER_H
#define CLASSIFIER_H

struct Symbols {
	int id;
	vector< vector< vector<float> > > referenceData;
};

class Classifier {
public:
	int parameters;
	vector<Symbols> TrainiedData;
	DTW dtw;

	pair<pair<int, float>, pair<int, float> > Classify(vector< vector<float> > &Inputdata);

	vector<float> statitics(vector< vector<float> > Gesture);

	float distance(vector<float> a, vector<float> b, int method = 0);
};
#endif