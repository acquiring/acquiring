#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <thread>
#include "dtw.h";
using namespace std;
#ifndef CLASSIFIERMULTITHREAD_H
#define CLASSIFIERMULTITHREAD_H
struct Symbols{
	int id;
	vector< vector< vector<float> > > refrenceData;
};
class ClassifierMultiThread {
public:
	static int parameters;
	static vector<Symbols> TrainiedData;
	static vector< vector<float> > dists;
	static DTW dtw;
	static vector< vector<float> > smaller(vector< vector<float> > &ins, int size){
		vector< vector<float> > outs;
		outs = ins;
		while (size-- >= 0){
			for (int i = 0; i < (int)outs.size() - 1; i++){
				for (int j = 0; j < outs[0].size(); j++){
					outs[i][j] = (outs[i][j] + outs[i + 1][j]) / 2;
				}
			}
			if((int)outs.size() > 0)
				outs.pop_back();
		}
		return outs;
	}
	static void serial(vector< vector<float> > *Inputdata,int i,int j)
	{
		dists[i][j] = dtw.dtw(smaller(*Inputdata, Inputdata->size() - TrainiedData[i].refrenceData[j].size()), TrainiedData[i].refrenceData[j], parameters);
	}
	pair<int, float> Classifiy(vector< vector<float> > &Inputdata){
		dists.clear();
		vector<thread> threads;
		float GlobalDistance = VERY_BIG;
		int id = -1;
		for (int i = 0; i < TrainiedData.size(); i++){
			dists.push_back(vector<float>());
			for (int j = 0; j < TrainiedData[i].refrenceData.size(); j++){
				dists[i].push_back(0);
				threads.push_back(std::thread(serial, &Inputdata, i, j));
				threads.back().join();
			}
		}
		//for (auto& t : threads) t.join();
		

		for (int i = 0; i < TrainiedData.size(); i++){
			float distance = VERY_BIG;
			for (int j = 0; j < TrainiedData[i].refrenceData.size(); j++){
				distance = min(distance, dists[i][j]);
			}
			if (distance < GlobalDistance){
				GlobalDistance = distance;
				id = TrainiedData[i].id;
			}
		}

		//for (auto& t : threads) t.detach();

		return make_pair(id, GlobalDistance);
	}
};
#endif