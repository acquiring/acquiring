#include "Classifier.h"

pair<pair<int, float>, pair<int, float>> Classifier::Classify(vector<vector<float>>& Inputdata) {
	float GlobalDistance = VERY_BIG;
	//float statsGlobalDistance = VERY_BIG;
	int id = -1;
	//int idStats = -1;
	//auto inputStats = this->statitics(Inputdata);
	for (int i = 0; i < (int)TrainiedData.size(); i++) {
		float distance = VERY_BIG;
		//float statDistance = VERY_BIG;
		for (int j = 0; j < (int)TrainiedData[i].referenceData.size(); j++) {
			distance = min(distance, dtw.dtw(Inputdata, TrainiedData[i].referenceData[j], parameters));
			//statDistance = min(statDistance, this->distance(inputStats, this->statitics(TrainiedData[i].referenceData[j]), 0));
		}

		if (distance < GlobalDistance) {
			GlobalDistance = distance;
			id = TrainiedData[i].id;
		}

		/*if (statDistance < statsGlobalDistance) {
		statsGlobalDistance = statDistance;
		idStats = TrainiedData[i].id;
		}*/

	}

	return make_pair(make_pair(id, GlobalDistance), make_pair(TrainiedData.size(), -1));
}

vector<float> Classifier::statitics(vector<vector<float>> Gesture) {
	vector<float> stats(12);
	for (int row = 0; row < (int)Gesture.size(); row++) {
		stats[0] = Gesture[0][0];
		stats[1] = Gesture[0][1];
		stats[2] = Gesture[0][2];
		stats[3] = Gesture[0][0];
		stats[4] = Gesture[0][1];
		stats[5] = Gesture[0][2];
		// maxX,maxY,maxZ
		stats[0] = max(stats[0], Gesture[row][0]);
		stats[1] = max(stats[1], Gesture[row][1]);
		stats[2] = max(stats[2], Gesture[row][2]);
		// minX,minY,minZ
		stats[3] = min(stats[3], Gesture[row][0]);
		stats[4] = min(stats[4], Gesture[row][1]);
		stats[5] = min(stats[5], Gesture[row][2]);
		// meanX,meanY,meanZ
		stats[6] += Gesture[row][0];
		stats[7] += Gesture[row][1];
		stats[8] += Gesture[row][2];
	}
	stats[6] /= (float)Gesture.size();
	stats[7] /= (float)Gesture.size();
	stats[8] /= (float)Gesture.size();
	// varX,varY,varZ
	for (int row = 0; row < (int)Gesture.size(); row++) {
		stats[9] += (stats[6] - Gesture[row][0])*(stats[6] - Gesture[row][0]);
		stats[10] += (stats[7] - Gesture[row][1])*(stats[7] - Gesture[row][1]);
		stats[11] += (stats[8] - Gesture[row][2])*(stats[8] - Gesture[row][2]);
	}

	stats[9] /= (float)Gesture.size();
	stats[10] /= (float)Gesture.size();
	stats[11] /= (float)Gesture.size();
	return stats;
}

float Classifier::distance(vector<float> a, vector<float> b, int method) {
	if (a.size() != b.size())
		return -1;

	//0 euclidean
	if (method == 0) {
		float sum = 0.0f;
		for (int i = 0; i < (int)a.size(); i++) {
			sum += (a[i] - b[i])*(a[i] - b[i]);
		}
		return sqrt(sum);
	}
	return -1;
}
