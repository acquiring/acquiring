#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "Classifier.h"
//#define VERY_BIGS  (float)(1e30)

//gesture and trial base 0
string getFileName(string path,int gesture, int trial, bool acc){
	char str[50];
	int j = (gesture * 3) + trial + 1;
	if (acc) {
		sprintf_s(str,"%saccOut_%d.txt" , path.c_str() , j);
	} else {
		sprintf_s(str, "%sgyroOut_%d.txt", path.c_str(), j);
	}
	return string(str);
}
//gesture and trial base 0
typedef vector< pair<long long, vector<float> > > GestureFile;
GestureFile  readData(string path, int gesture,int trial,bool acc){
	GestureFile f;
	ifstream myfile(getFileName(path, gesture, trial, acc).c_str());
	if (myfile.is_open())
	{
		string line;
		getline(myfile, line);
		float x, y, z;
		long long epoch;
		//while (myfile >> x >> y >> z >>epoch)
		while(getline(myfile, line))
		{
			sscanf_s(line.c_str(), "%f, %f, %f, %lld", &x, &y, &z, &epoch);
			f.push_back(make_pair(epoch,vector<float>{ x, y, z }));
		}
		myfile.close();
	}
	return f;
}

vector<vector<float> > Merge(GestureFile acc , GestureFile gyro) {
	vector<vector<float> > temp;
	for (int i = 0; i < (int)acc.size(); i++) {
		for (int j = 0; j < (int)gyro.size(); j++) {
			if (acc[i].first == gyro[j].first) {
				vector<float> both;
				both.insert(both.end(), acc[i].second.begin(), acc[i].second.end());
				both.insert(both.end(), gyro[j].second.begin(), gyro[j].second.end());
				temp.push_back(both);
			}
			
		}
	}
	return temp;
}
int linesTable[7][8][10];
int main(){
	string path = "C:\\Users\\Maged\\Pictures\\";
	Classifier classifier;
	classifier.parameters = 6;
	int trs = 0;
	for (int gesture = 0; gesture < 6; gesture++){
		for (int trial = 0; trial < 1; trial++) {
			Symbols s;
			s.id = gesture;
			s.referenceData.push_back(Merge(readData(path,gesture, trial, true), readData(path, gesture, trial,false)));
			classifier.TrainiedData.push_back(s);
		}
	}
	double correct = 0, total = 0;
	for (int i = 0; i < 6; i++){
		for (int j = 0; j < 3; j++){
			float ans = VERY_BIG;
			vector<vector<float > > temp = Merge(readData(path, i, j, true), readData(path, i, j, false));
			int id = classifier.Classify(temp).first.first;
			total++;
			if (id == i) correct++;
			printf("%.0lf %.0lf\n", correct, total);
		}
		
	}
	printf("%.2lf\n", correct * 100 / total);
	//scanf_s("%d", (int)correct);
	return 0;
}