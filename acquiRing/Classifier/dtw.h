#include <stdio.h>
#include <stdlib.h>
#include <vector>
using namespace std;
#define VERY_BIG  (float)(1e30)
#ifndef DTW_H
#define DTW_H
class DTW{
public:
	float dtw(vector< vector<float> > x, vector<vector<float> > y, unsigned int params);
};
#endif