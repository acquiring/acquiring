#include "Gesture.h"
#include "Classifier\ClassifierAdapter.h"
#include "Util.h"
#include "GUI\MainPage.xaml.h"
#include "Plugin.h"

using namespace acquiRing;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::Data::Json;


Gesture::Gesture() {
}

acquiRing::Gesture::Gesture(int id, String ^ name) {
	this->ID = id;
	this->Name = name->IsEmpty() ? "no_name" : name;
	this->Deleted = true;
}

Vector<Gesture^>^ Gesture::m_gestureList = ref new Vector<Gesture^>();

void acquiRing::Gesture::Delete() {
	this->Deleted = true;

	auto storageFolder = ApplicationData::Current->LocalFolder;

	int trials = ClassifierAdapter::getInstance()->getTrials();
	String^ fname;
	for (int i = 0; i < trials; i++) {
		fname = ss2ps(ClassifierAdapter::getInstance()->getFileName(ID, i, true));
		create_task(storageFolder->CreateFileAsync(fname, CreationCollisionOption::OpenIfExists))
			.then([](StorageFile^ sampleFile) {
			sampleFile->DeleteAsync();
		});

		fname = ss2ps(ClassifierAdapter::getInstance()->getFileName(ID, i, false));
		create_task(storageFolder->CreateFileAsync(fname, CreationCollisionOption::OpenIfExists))
			.then([](StorageFile^ sampleFile) {
			sampleFile->DeleteAsync();
		});
	}

	Gesture::Save();

	ClassifierAdapter::getInstance()->TrainGestures();

	MainPage::UpdateGestureListView();
}

int Gesture::Load() {
	auto gestureStr = LoadTxtFile("gestures.json");
	if (gestureStr.empty()) return 0;

	JsonArray^ mapValue = ref new JsonArray();
	if (JsonArray::TryParse(ss2ps(gestureStr), &mapValue)) {
		auto vec = mapValue->GetView();

		Gesture::GestureList->Clear();
		//Plugin^ defaultPlugin = PluginManager::Instance->PluginList->First()->Current;
		std::for_each(begin(vec), end(vec), [](IJsonValue^ M) {
			auto gesture = ref new Gesture(
				(int)M->GetObject()->GetNamedNumber("ID"),
				M->GetObject()->GetNamedString("NAME"));
			gesture->Undelete();
			//gesture->Operation = defaultPlugin;

			Gesture::GestureList->Append(gesture);
		});

	}

	MainPage::UpdateGestureListView();

	return Gesture::GestureList->Size;
}

void Gesture::Save() {
	JsonArray^ jarray = ref new JsonArray();
	for each (auto gesture in Gesture::GestureList) {
		if (gesture->Deleted == false) {
			JsonObject^ jobj = ref new JsonObject();
			jobj->Insert("ID", JsonValue::CreateNumberValue(gesture->ID));
			jobj->Insert("NAME", JsonValue::CreateStringValue(gesture->Name));
			jarray->Append(jobj);
		}
	}

	String^ jsonString = jarray->Stringify();

	SaveTxtFile("gestures.json", jsonString);

	MainPage::UpdateGestureListView();
}

Gesture^ acquiRing::Gesture::getGestureByID(int gID) {
	for each (auto gesture in Gesture::GestureList) {
		if (gID == gesture->ID) return gesture;
	}
	return nullptr;
}
