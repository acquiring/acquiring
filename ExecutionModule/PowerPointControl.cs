﻿using System;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;
using System.Threading;

namespace ExecutionModule {
    class PowerPointControl {
        private Application application;
        private Presentation presentation;
        private DocumentWindow window;

        public PowerPointControl() {
        }

        private static PowerPointControl ppt;
        public static void Execute(DS.EXEC_CODE code) {
            if (ppt == null) {
                ppt = new PowerPointControl();
                if (ppt.findOpenedPresentation() == false) {
                    ppt = null;
                    return;
                }
            }

            switch (code) {
                case DS.EXEC_CODE.PPT_NEXT:
                    ppt.goToNextSlide();
                    break;
                case DS.EXEC_CODE.PPT_PREVIOUS:
                    ppt.goToPreviousSlide();
                    break;
                case DS.EXEC_CODE.PPT_ZOOMIN:
                    ppt.zoomIn();
                    break;
                case DS.EXEC_CODE.PPT_ZOOMOUT:
                    ppt.zoomOut(); ;
                    break;
                default:
                    break;
            }
        }

        public Boolean findOpenedPresentation() {
            try {
                application = Marshal.GetActiveObject("PowerPoint.Application") as Application;
            } catch {
                return false;
            }
            if (application == null) return false;

            try {
                this.window = application.ActiveWindow;
            } catch { }
            this.presentation = application.ActivePresentation;

            return true;
        }

        public Boolean preparePresentation(String fileName) {
            application = new Application();
            try {
                presentation = application.Presentations.Open2007(fileName, MsoTriState.msoTrue, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoTrue);
                writeToConsoleAndOutput("Open presentation");

                return true;
            } catch (Exception e) {
                writeToConsoleAndOutput("Power point open returned a exception message: " + e.Message);
                return false;
            }
        }

        public int currentSlide() {
            int cnt;
            try {
                cnt = presentation.SlideShowWindow.View.CurrentShowPosition;
            } catch {
                cnt = window.View.Slide.SlideIndex;
            }
            return cnt;
        }

        public void goToFirstSlide() {
            try {
                presentation.SlideShowWindow.View.First();
            } catch {
                window.View.GotoSlide(1);
            }
        }

        public void goToLastSlide() {
            try {
                presentation.SlideShowWindow.View.Last();
            } catch {
                window.View.GotoSlide(totalSlides());
            }
        }

        public void goToSlideNumber(int slideNumber) {
            if (slideNumber > totalSlides()) return;

            try {
                presentation.SlideShowWindow.View.GotoSlide(slideNumber);
            } catch {
                window.View.GotoSlide(slideNumber);
            }
        }

        public void zoomIn() {
            try {
                var zm = presentation.SlideShowWindow.View.Zoom;
                System.Windows.Forms.SendKeys.SendWait("^{+}");
                
            } catch {
                try {
                    if (window.View.Zoom + 10 <= 400)
                        window.View.Zoom += 10;
                } catch (Exception ex) {
                    Console.Out.WriteLine(ex.Message);
                }
            }
        }

        public void zoomOut() {
            try {
                var zm = presentation.SlideShowWindow.View.Zoom;
                System.Windows.Forms.SendKeys.SendWait("^{-}");
            } catch {
                try {
                    
                    if (window.View.Zoom - 10 >= 10)
                        window.View.Zoom -= 10;
                } catch (Exception ex) {
                    Console.Out.WriteLine(ex.Message);
                }
            }
        }

        public void startPresentation() {
            SlideShowSettings sst = presentation.SlideShowSettings;
            sst.ShowType = Microsoft.Office.Interop.PowerPoint.PpSlideShowType.ppShowTypeSpeaker;
            sst.Run();
        }

        public void goToNextSlide() {
            try {
                presentation.SlideShowWindow.View.Next();
            } catch {
                int current = currentSlide();
                if (current < totalSlides())
                    window.View.GotoSlide(current + 1);
            }
        }

        public void goToPreviousSlide() {
            try {
                presentation.SlideShowWindow.View.Previous();
            } catch {
                int current = currentSlide();
                if (current > 1)
                    window.View.GotoSlide(current - 1);
            }

        }

        public int totalSlides() {
            return presentation.Slides.Count;
        }

        public Boolean closePresentation() {
            try {
                presentation.Close();
                return true;
            } catch (Exception e) {
                writeToConsoleAndOutput("Power point close returned a exception message: " + e.Message);
                return false;
            }
        }

        public void writeToConsoleAndOutput(String message) {
            System.Diagnostics.Debug.Write("\n\n" + message + "\n\n");
            Console.WriteLine("\n\n" + message + "\n\n");
        }

        public static void Test(System.Windows.Forms.Keys key) {
            PowerPointControl ppt = new PowerPointControl();
            ppt.findOpenedPresentation();
            //ppt.startPresentation();
            Thread.Sleep(500);
            ppt.goToNextSlide();

            ppt.zoomIn();
            ppt.zoomIn();
            ppt.zoomIn();
            ppt.zoomIn();

            Thread.Sleep(500);
            ppt.window.SmallScroll(0, 0, 1, 0);
            ppt.window.SmallScroll(0, 0, 1, 0);
            ppt.window.SmallScroll(0, 0, 1, 0);
            ppt.window.SmallScroll(0, 0, 1, 0);
            ppt.window.LargeScroll(0, 0, 1, 0);
            ppt.window.LargeScroll(0, 0, 1, 0);

            //Thread.Sleep(500);
            //ppt.goToNextSlide();

            //Thread.Sleep(500);
            //ppt.goToNextSlide();

            //Thread.Sleep(500);
            //ppt.goToPreviousSlide();

            //Thread.Sleep(500);
            //ppt.goToPreviousSlide();

            //Thread.Sleep(500);
            //ppt.goToPreviousSlide();

            //Thread.Sleep(500);
            //ppt.zoomIn();

            //Thread.Sleep(500);
            //ppt.zoomIn();

            //Thread.Sleep(500);
            //ppt.zoomOut();

            //Thread.Sleep(500);
            //ppt.zoomOut();
        }
    }
}
