﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Drawing;

namespace ExecutionModule {
    class SubwayController {
        public static void Execute(DS.EXEC_CODE code) {
            switch (code) {
                case DS.EXEC_CODE.SUBWAY_LEFT:
                    SubwayController.goLeft();
                    break;
                case DS.EXEC_CODE.SUBWAY_RIGHT:
                    SubwayController.goRight();
                    break;
                case DS.EXEC_CODE.SUBWAY_UP:
                    SubwayController.goUp();
                    break;
                case DS.EXEC_CODE.SUBWAY_DOWN:
                    SubwayController.goDown();
                    break;
                default:
                    break;
            }
        }

        private static void goLeft() {
            Point c = new Point();
            Win32API.GetCursorPos(out c);

            MouseControl.DoMouseDown(true);
            MouseControl.MoveMouse(c.X, c.Y, -100, 0);
            MouseControl.DoMouseUp(true);

            MouseControl.MoveMouse(c.X, c.Y, 0, 0);
        }

        private static void goRight() {
            Point c = new Point();
            Win32API.GetCursorPos(out c);


            MouseControl.DoMouseDown(true);
            MouseControl.MoveMouse(c.X, c.Y, 100, 0);
            MouseControl.DoMouseUp(true);

            MouseControl.MoveMouse(c.X, c.Y, 0, 0);
        }

        private static void goUp() {
            Point c = new Point();
            Win32API.GetCursorPos(out c);

            MouseControl.DoMouseDown(true);
            MouseControl.MoveMouse(c.X, c.Y, 0, -100);
            MouseControl.DoMouseUp(true);

            MouseControl.MoveMouse(c.X, c.Y, 0, 0);
        }

        private static void goDown() {
            Point c = new Point();
            Win32API.GetCursorPos(out c);

            MouseControl.DoMouseDown(true);
            MouseControl.MoveMouse(c.X, c.Y, -0, 100);
            MouseControl.DoMouseUp(true);

            MouseControl.MoveMouse(c.X, c.Y, 0, 0);
        }

        public static void Test() {
            Thread.Sleep(500);
            goUp();

            Thread.Sleep(500);
            goUp();

            Thread.Sleep(500);
            goUp();

            Thread.Sleep(500);
            goUp();
        }
    }
}
