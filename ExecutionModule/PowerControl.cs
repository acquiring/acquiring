﻿using System.Windows.Forms;
using System.Diagnostics;

namespace ExecutionModule {
    class PowerControl {
        public static void Execute(DS.EXEC_CODE code) {
            switch (code) {
                case DS.EXEC_CODE.SHUTDOWN:
                    shutdown();
                    break;
                case DS.EXEC_CODE.RESTART:
                    restart();
;                    break;
                case DS.EXEC_CODE.LOGOFF:
                    logoff();
                    break;
                case DS.EXEC_CODE.LOCK:
                    lockScreen();
                    break;
                case DS.EXEC_CODE.SLEEP:
                    sleep();
                    break;
                case DS.EXEC_CODE.HIBERNATE:
                    hibernate();
                    break;
                default:
                    break;
            }
        }

        public static void shutdown() {
            Process.Start("shutdown", "/s /t 0");
        }

        public static void restart() {
            Process.Start("shutdown", "/r /t 0");
        }

        public static void logoff() {
            Win32API.ExitWindowsEx(0, 0);
        }

        public static void lockScreen() {
            Win32API.LockWorkStation();
        }

        public static void sleep() {
            Application.SetSuspendState(PowerState.Suspend, false, true);
        }

        public static void hibernate() {
            Application.SetSuspendState(PowerState.Hibernate, false, true);
        }
    }
}
