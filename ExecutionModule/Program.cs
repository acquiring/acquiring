﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

namespace ExecutionModule {
    class Program {

        static void test(Keys key) {
            switch (key) {
                case Keys.W:
                    SubwayController.Execute(DS.EXEC_CODE.SUBWAY_UP);
                    break;
                case Keys.S:
                    SubwayController.Execute(DS.EXEC_CODE.SUBWAY_DOWN);
                    break;
                case Keys.A:
                    SubwayController.Execute(DS.EXEC_CODE.SUBWAY_LEFT);
                    break;
                case Keys.D:
                    SubwayController.Execute(DS.EXEC_CODE.SUBWAY_RIGHT);
                    break;
                default:
                    break;
            }
        }
        
        static void Main(string[] args) {
            //InterceptKeyboard.Run(PowerPointControl.Test);

            if (args.Count() == 0) return;
            if (File.Exists(args[0]) == false) return;

            var newArgs = File.ReadAllLines(args[0]);
            foreach (var arg in newArgs) {
                Console.Out.WriteLine(arg); // DEBUG

                var val = arg.Split(',');
                int codeInt;
                if (Int32.TryParse(val[0], out codeInt) == false) return;

                var code = (DS.EXEC_CODE)codeInt;
                switch (code) {
                    case DS.EXEC_CODE.PPT_NEXT:
                    case DS.EXEC_CODE.PPT_PREVIOUS:
                    case DS.EXEC_CODE.PPT_ZOOMIN:
                    case DS.EXEC_CODE.PPT_ZOOMOUT:
                        PowerPointControl.Execute(code);
                        break;
                    case DS.EXEC_CODE.MEDIA_PLAYER_PLAY_PAUSE:
                    case DS.EXEC_CODE.MEDIA_PLAYER_STOP:
                    case DS.EXEC_CODE.MEDIA_PLAYER_PREVIOUS:
                    case DS.EXEC_CODE.MEDIA_PLAYER_NEXT:
                    case DS.EXEC_CODE.MEDIA_PLAYER_VOLUMEUP:
                    case DS.EXEC_CODE.MEDIA_PLAYER_VOLUMEDOEN:
                    case DS.EXEC_CODE.MEDIA_PLAYER_MUTE:
                        MediaPlayerControl.Execute(code);
                        break;
                    case DS.EXEC_CODE.KEY:
                        if (val.Count() < 2) return;
                        SendKeys.SendWait(val[1]);
                        break;
                    case DS.EXEC_CODE.MOUSE_LEFT_CLICK:
                    case DS.EXEC_CODE.MOUSE_RIGHT_CLICK:
                    case DS.EXEC_CODE.MOUSE_MOVE:
                        MouseControl.Execute(code);
                        break;
                    case DS.EXEC_CODE.SHUTDOWN:
                    case DS.EXEC_CODE.RESTART:
                    case DS.EXEC_CODE.LOGOFF:
                    case DS.EXEC_CODE.LOCK:
                    case DS.EXEC_CODE.SLEEP:
                    case DS.EXEC_CODE.HIBERNATE:
                        PowerControl.Execute(code);
                        break;
                    case DS.EXEC_CODE.SUBWAY_LEFT:
                    case DS.EXEC_CODE.SUBWAY_RIGHT:
                    case DS.EXEC_CODE.SUBWAY_UP:
                    case DS.EXEC_CODE.SUBWAY_DOWN:
                        SubwayController.Execute(code);
                        break;
                    default:
                        break;
                }
            }

            //WindowsInput.InputSimulator.SimulateKeyPress(WindowsInput.VirtualKeyCode.NUMLOCK);
            //var iandle = Win32API.FindWindow("WMPlayerApp", "Windows Media Player");
            //Win32API.ShowWindow(iandle, 3);
            //Win32API.SetForegroundWindow(iandle);

            //Console.In.ReadLine();   // DEBUG
        }
    }
}
