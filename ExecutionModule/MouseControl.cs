﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Threading;

namespace ExecutionModule {
    class MouseControl {
        public static void Execute(DS.EXEC_CODE code) {
            switch (code) {
                case DS.EXEC_CODE.MOUSE_LEFT_CLICK:
                    MouseControl.DoMouseClick(true);
                    break;
                case DS.EXEC_CODE.MOUSE_RIGHT_CLICK:
                    MouseControl.DoMouseClick(false);
                    break;
                case DS.EXEC_CODE.MOUSE_MOVE:
                    MouseControl.MoveMouse(0, 0, 0, 0);
                    break;
            }
        }

        public static void DoMouseClick(bool isLeft) {
            //Call the imported function with the cursor's current position
            int X = Cursor.Position.X;
            int Y = Cursor.Position.Y;
            if (isLeft)
                Win32API.mouse_event(Win32API.MOUSEEVENTF_LEFTDOWN | Win32API.MOUSEEVENTF_LEFTUP, X, Y, 0, 0);
            else
                Win32API.mouse_event(Win32API.MOUSEEVENTF_RIGHTDOWN | Win32API.MOUSEEVENTF_RIGHTUP, X, Y, 0, 0);
        }

        public static void DoMouseDown(bool isLeft) {
            //Call the imported function with the cursor's current position
            int X = Cursor.Position.X;
            int Y = Cursor.Position.Y;
            if (isLeft)
                Win32API.mouse_event(Win32API.MOUSEEVENTF_LEFTDOWN, X, Y, 0, 0);
            else
                Win32API.mouse_event(Win32API.MOUSEEVENTF_RIGHTDOWN, X, Y, 0, 0);
        }

        public static void DoMouseUp(bool isLeft) {
            //Call the imported function with the cursor's current position
            int X = Cursor.Position.X;
            int Y = Cursor.Position.Y;
            if (isLeft)
                Win32API.mouse_event(Win32API.MOUSEEVENTF_LEFTUP, X, Y, 0, 0);
            else
                Win32API.mouse_event(Win32API.MOUSEEVENTF_RIGHTUP, X, Y, 0, 0);
        }

        public static void LinearSmoothMove(Point newPosition, int steps) {
            Point start;
            Win32API.GetCursorPos(out start);
            PointF iterPoint = start;
            int MouseEventDelayMS = 20;

            // Find the slope of the line segment defined by start and newPosition
            PointF slope = new PointF(newPosition.X - start.X, newPosition.Y - start.Y);

            // Divide by the number of steps
            slope.X = slope.X / steps;
            slope.Y = slope.Y / steps;

            // Move the mouse to each iterative point.
            for (int i = 0; i < steps; i++) {
                iterPoint = new PointF(iterPoint.X + slope.X, iterPoint.Y + slope.Y);
                Point p = Point.Round(iterPoint);
                Win32API.SetCursorPos(p.X, p.Y);
                Thread.Sleep(MouseEventDelayMS);
            }

            // Move the mouse to the final destination.
            Win32API.SetCursorPos(newPosition.X, newPosition.Y);
        }

        public static void LinearSmoothMove_2(Point newPosition, TimeSpan duration) {
            /*
            TEST
                TimeSpan delayt = new  TimeSpan(0, 0, 3);
                LinearSmoothMove_2(new Point(20, 40), delayt);
            */
            Point start;
            Win32API.GetCursorPos(out start);

            // Find the vector between start and newPosition
            float deltaX = newPosition.X - start.X;
            float deltaY = newPosition.Y - start.Y;

            // start a timer
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            float timeFraction = 0.0f;

            do {
                timeFraction = (float)stopwatch.Elapsed.Ticks / duration.Ticks;
                if (timeFraction > 1.0f)
                    timeFraction = 1.0f;

                PointF curPoint = new PointF(start.X + timeFraction * deltaX,
                                             start.Y + timeFraction * deltaY);
                Point p = Point.Round(curPoint);
                Win32API.SetCursorPos(p.X, p.Y);
                Thread.Sleep(20);
            } while (timeFraction < 1.0);
        }

        static Random random = new Random();
        static int mouseSpeed = 15;

        public static void MoveMouse(int x, int y, int rx, int ry) {
            /*
            TEST
             MoveMouse(0, 0, 0, 0);
            */
            Point c = new Point();
            Win32API.GetCursorPos(out c);

            //x += random.Next(rx);
            //y += random.Next(ry);
            x += rx;
            y += ry;

            double randomSpeed = Math.Max((random.Next(mouseSpeed) / 2.0 + mouseSpeed) / 10.0, 0.1);

            WindMouse(c.X, c.Y, x, y, 9.0, 3.0, 10.0 / randomSpeed,
                15.0 / randomSpeed, 10.0 * randomSpeed, 10.0 * randomSpeed);
        }

        static void WindMouse(double xs, double ys, double xe, double ye,
            double gravity, double wind, double minWait, double maxWait,
            double maxStep, double targetArea) {

            double dist, windX = 0, windY = 0, veloX = 0, veloY = 0, randomDist, veloMag, step;
            int oldX, oldY, newX = (int)Math.Round(xs), newY = (int)Math.Round(ys);

            double waitDiff = maxWait - minWait;
            double sqrt2 = Math.Sqrt(2.0);
            double sqrt3 = Math.Sqrt(3.0);
            double sqrt5 = Math.Sqrt(5.0);

            dist = Hypot(xe - xs, ye - ys);

            while (dist > 1.0) {

                wind = Math.Min(wind, dist);

                if (dist >= targetArea) {
                    int w = random.Next((int)Math.Round(wind) * 2 + 1);
                    windX = windX / sqrt3 + (w - wind) / sqrt5;
                    windY = windY / sqrt3 + (w - wind) / sqrt5;
                } else {
                    windX = windX / sqrt2;
                    windY = windY / sqrt2;
                    if (maxStep < 3)
                        maxStep = random.Next(3) + 3.0;
                    else
                        maxStep = maxStep / sqrt5;
                }

                veloX += windX;
                veloY += windY;
                veloX = veloX + gravity * (xe - xs) / dist;
                veloY = veloY + gravity * (ye - ys) / dist;

                if (Hypot(veloX, veloY) > maxStep) {
                    randomDist = maxStep / 2.0 + random.Next((int)Math.Round(maxStep) / 2);
                    veloMag = Hypot(veloX, veloY);
                    veloX = (veloX / veloMag) * randomDist;
                    veloY = (veloY / veloMag) * randomDist;
                }

                oldX = (int)Math.Round(xs);
                oldY = (int)Math.Round(ys);
                xs += veloX;
                ys += veloY;
                dist = Hypot(xe - xs, ye - ys);
                newX = (int)Math.Round(xs);
                newY = (int)Math.Round(ys);

                if (oldX != newX || oldY != newY)
                    Win32API.SetCursorPos(newX, newY);

                step = Hypot(xs - oldX, ys - oldY);
                int wait = (int)Math.Round(waitDiff * (step / maxStep) + minWait);
                Thread.Sleep(wait);
            }

            int endX = (int)Math.Round(xe);
            int endY = (int)Math.Round(ye);
            if (endX != newX || endY != newY)
                Win32API.SetCursorPos(endX, endY);
        }

        static double Hypot(double dx, double dy) {
            return Math.Sqrt(dx * dx + dy * dy);
        }
    }
}
