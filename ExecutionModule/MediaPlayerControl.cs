﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
//using Microsoft.MediaPlayer.Interop;

namespace ExecutionModule {
    class MediaPlayerControl {
        public static void Execute(DS.EXEC_CODE code) {
            switch (code) {
                case DS.EXEC_CODE.MEDIA_PLAYER_PLAY_PAUSE:
                    PlayPause();
                    break;
                case DS.EXEC_CODE.MEDIA_PLAYER_STOP:
                    Stop();
                    break;
                case DS.EXEC_CODE.MEDIA_PLAYER_PREVIOUS:
                    Previous();
                    break;
                case DS.EXEC_CODE.MEDIA_PLAYER_NEXT:
                    Next();
                    break;
                case DS.EXEC_CODE.MEDIA_PLAYER_VOLUMEUP:
                    VolumeUp();
                    break;
                case DS.EXEC_CODE.MEDIA_PLAYER_VOLUMEDOEN:
                    VolumeDown();
                    break;
                case DS.EXEC_CODE.MEDIA_PLAYER_MUTE:
                    Mute();
                    break;
                default:
                    break;
            }
        }

        
        public MediaPlayerControl() {            
            
        }

        public static int FindWindow() {
            return Win32API.FindWindow("WMPlayerApp", "Windows Media Player");
        }

        public static void PlayPause() {
            var iHandle = FindWindow();
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x00004978, 0x00000000);
        }

        public static void Stop() {
            var iHandle = FindWindow();
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x00004979, 0x00000000);
        }

        public static void Previous() {
            var iHandle = FindWindow();
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x0000497A, 0x00000000);
        }

        public static void Next() {
            var iHandle = FindWindow();
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x0000497B, 0x00000000);
        }

        public static void VolumeUp() {
            var iHandle = FindWindow();
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x0000497F, 0x00000000);
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x0000497F, 0x00000000);
        }

        public static void VolumeDown() {
            var iHandle = FindWindow();
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x00004980, 0x00000000);
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x00004980, 0x00000000);
        }

        public static void Mute() {
            var iHandle = FindWindow();
            Win32API.SendMessage(iHandle, Win32API.WM_COMMAND, 0x00004981, 0x00000000);
        }     
    }
}
