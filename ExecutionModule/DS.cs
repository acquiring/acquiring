﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExecutionModule {
    public static class DS {
        public enum EXEC_CODE {
            PPT_NEXT,
            PPT_PREVIOUS,
            PPT_ZOOMIN,
            PPT_ZOOMOUT,
            MEDIA_PLAYER_PLAY_PAUSE,
            MEDIA_PLAYER_STOP,
            MEDIA_PLAYER_PREVIOUS,
            MEDIA_PLAYER_NEXT,
            MEDIA_PLAYER_VOLUMEUP,
            MEDIA_PLAYER_VOLUMEDOEN,
            MEDIA_PLAYER_MUTE,
            KEY,
            MOUSE_LEFT_CLICK,
            MOUSE_RIGHT_CLICK,
            MOUSE_MOVE,
            SHUTDOWN,
            RESTART,
            LOGOFF,
            LOCK,
            SLEEP,
            HIBERNATE,
            SUBWAY_LEFT,
            SUBWAY_RIGHT,
            SUBWAY_UP,
            SUBWAY_DOWN,
        };
    }
}
